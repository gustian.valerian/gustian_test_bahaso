function buttonHapus(idx, url) {
        swal.fire({
                title: 'Apa Kamu Yakin ?',
                text: "Anda tidak akan dapat mengembalikan ini!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Ya',
                cancelButtonText: 'Tidak',
                reverseButtons: true
            }).then(function (result) {
                if (result.value) {
                    // alert(idx + url + 'celete nih ye');
                    // console.log('delete');
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $("input[name='_token']").val()
                        }
                    });
                    let href = base_url + '/'+ url + '/'+ idx;
                    // console.log(href);
                    $.ajax({
                        type: "DELETE",
                        url: href,
                        data: {
                        },
                        success: function (data) {
                            jQuery.notify(
                                {title: 'Hapus!', message: 'File Anda telah berhasil dihapus.'},
                                {
                                  type: 'danger',
                                  delay: 5000,
                                },
                              );
                            jQuery('#'+datatable.table_id).DataTable().ajax.reload();
                        }
                    });
            }
        });
}


function buttonaja(){
    jQuery.notify(
        {title: 'Hapus!', message: 'File Anda telah berhasil dihapus.'},
        {
          type: 'danger',
          delay: 5000,
        },
      );
}

function custommessage(ptitle, pmessage, ptype){
    jQuery.notify(
        {title: ptitle, message: pmessage},
        {
          type: ptype,
          delay: 1000,
        },
    );
}