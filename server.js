const io = require('socket.io-client');
const express = require('express');
const app = express();
const port = 8443;

const ioOptions = {
    autoConnect: false,
    rejectUnauthorized: false // needed when using a self signed cert
};

const apiAddress = 'https://103.144.15.162:8443/mt/api/rt/v1';

const socket = io(apiAddress, ioOptions)
    .on('connect', () => console.log('connected'))
    .on('connect_error', error => console.log('connect_error):', error))
    .on('error', error => console.log('error:', error));

console.log('open connection');
socket.open();

// login to get a valid session...
const session = 'AYEAfv8KEJsEVkXXLUF0qojKDkX-3sASFgoU3istJk0HPU0Fcf7Hfxv2D2wrSxkaEDLIbf_BfU_GhOkLFLfQ3ZIiEGS5PNNyk0ugryn2kT8DkWkqDkNDVFYtUExOLVBVU0FUMiEaEE2erIRywEb_v4iN8kVXy1cqDWFkbWluaXN0cmF0b3I';
// const topics = ['HELLO', 'HEARTBEAT', 'ALARM_OBJECTS', 'CAMERA_OBJECTS', 'SERVER_OBJECTS', 'SITE', 'END_OF_CATALOG', 'EVENT'];
const topics = ['EVENT'];

// The subtopics are only available for 'EVENT' topic
const subtopics = {
  EVENT: {
    // DEPRECATED: The usage of whitelist/blacklist will be deprecated in ACC 8
    // use include to include specific events
    // use exclude to exclude specific events
    include: ['DEVICE']
  }  
};

socket.emit(
    'sub-notification',
    { session, topics, subtopics, getSiteWideEvents: false },
    response => console.log('sub-notification:\n', JSON.stringify(response, undefined, '  '))
);

socket.on('notification', (notification, callback) => {
    console.log('notification:\n', JSON.stringify(notification, undefined, '  '));
    // console.log('notification:\n', JSON.stringify(notification.siteId, undefined, '  '));
    callback(); // invoking callback(false) will stop the notifications
});


app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})