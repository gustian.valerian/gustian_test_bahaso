<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/login', 'Auth\LoginController@login')->name('LoginApi');


// Execute in bellow if you add new api to swagger form
// php artisan l5-swagger:generate
Route::group(['middleware' => 'auth:api'], function () {
    Route::get('/artikel/list', 'Pengaturan\ArtikelController@list')->name('artikel_view_list');
    // Route::post('/artikel/{id}/update', 'Pengaturan\ArtikelController@update')->name('artikel_update_update');
    Route::resource('artikel', 'Pengaturan\ArtikelController', [
        'names' => [
            'index'     => 'artikel_view_index',
            'show'      => 'artikel_view_show',
            'store'     => 'artikel_create_store',
            'update'    => 'artikel_update_update',
            'destroy'   => 'artikel_delete_destroy',
            ]
        ]);
});