<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/cek/{id}', 'Auth\ProfileController@getSertifikatObjectCeck');

Route::get('/', function () {
    return redirect('/login');
});


Route::get('/template', function () {
    return view('template.acorn.sample.sample');
})->name('template');
Route::get('/template/index', function () {
    return view('template.acorn.sample.index');
})->name('template_index');


Route::get('/guest_template', function () {
    return view('template.guest_metronic');
});




Route::group(['middleware' => ['auth']], function () {
    Route::get('/dashboard', function () {
        return view('dashboard');
    })->name('dashboard');

    Route::get('/sertifikat', 'Auth\ProfileController@getSertifikat')->name('profile_view_sertifikatlist');
    Route::get('/sertifikat/list', 'Auth\ProfileController@getSertifikatList')->name('profile_view_sertifikatlist');
    Route::get('/sertifikat/{id}', 'Auth\ProfileController@getSertifikatObject')->name('profile_view_sertifikatobject');

   
    Route::group(['middleware' => 'dynamicprivilege'], function () {

        Route::get('/useraccount/list', 'Pengguna\MasterUserController@list')->name('useraccount_view_list');
        Route::get('/useraccount/import', 'Pengguna\MasterUserController@import')->name('useraccount_import_import');
        Route::get('/useraccount/export', 'Pengguna\MasterUserController@export')->name('useraccount_export_export');
        Route::resource('useraccount', 'Pengguna\MasterUserController', [
            'names' => [
                'index'     => 'useraccount_view_index',
                'show'      => 'useraccount_view_show',
                'create'    => 'useraccount_create_create', // useraccount/create with methode GET
                'store'     => 'useraccount_create_store', // useraccount with methode POST
                'edit'      => 'useraccount_update_edit', // useraccount/{{id}}/edit with methode GET
                'update'    => 'useraccount_update_update', // useraccount/{{id}} with methode PUT
                'destroy'   => 'useraccount_delete_destroy', // useraccount/{{id}} with methode DELETE
            ]
        ]);

        Route::get('/userakses/list', 'Pengguna\MapGroupMenuController@list')->name('userakses_view_list');
        Route::resource('userakses', 'Pengguna\MapGroupMenuController', [
            'names' => [
                'index'     => 'userakses_view_index',
                'show'      => 'userakses_view_show',
                'create'    => 'userakses_create_create',
                'store'     => 'userakses_create_store',
                'edit'      => 'userakses_update_edit',
                'update'    => 'userakses_update_update',
                'destroy'   => 'userakses_delete_destroy',
            ]
        ]);

        Route::get('/pendidikan/list', 'Pengguna\PendidikanController@list')->name('pendidikan_view_list');
        Route::resource('pendidikan', 'Pengguna\PendidikanController', [
            'names' => [
                'index'     => 'pendidikan_view_index',
                'show'      => 'pendidikan_view_show',
                'create'    => 'pendidikan_create_create',
                'store'     => 'pendidikan_create_store',
                'edit'      => 'pendidikan_update_edit',
                'update'    => 'pendidikan_update_update',
                'destroy'   => 'pendidikan_delete_destroy',
            ]
        ]);
        

        Route::get('/pekerjaan/list', 'Pengguna\PekerjaanController@list')->name('pekerjaan_view_list');
        Route::resource('pekerjaan', 'Pengguna\PekerjaanController', [
            'names' => [
                'index'     => 'pekerjaan_view_index',
                'show'      => 'pekerjaan_view_show',
                'create'    => 'pekerjaan_create_create',
                'store'     => 'pekerjaan_create_store',
                'edit'      => 'pekerjaan_update_edit',
                'update'    => 'pekerjaan_update_update',
                'destroy'   => 'pekerjaan_delete_destroy',
            ]
        ]);

        Route::get('/artikel/list', 'Pengaturan\ArtikelController@list')->name('artikel_view_list');
        Route::resource('artikel', 'Pengaturan\ArtikelController', [
            'names' => [
                'index'     => 'artikel_view_index',
                'show'      => 'artikel_view_show',
                'create'    => 'artikel_create_create',
                'store'     => 'artikel_create_store',
                'edit'      => 'artikel_update_edit',
                'update'    => 'artikel_update_update',
                'destroy'   => 'artikel_delete_destroy',
            ]
        ]);
    });
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
