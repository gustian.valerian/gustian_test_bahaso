<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\MasterPeserta;
use Illuminate\Support\Str;

class MasterUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    function __construct()
    {
        $this->mastergroup  = new App\Models\MasterGroup;
        // $this->masterrole = new App\Models\MasterRole;
        // $this->masterpeserta = new App\Models\MasterPeserta;
        // $this->masterdivisi = new App\Models\MasterDivisi;
    }

    public function run()
    {

        $group = $this->mastergroup->getobject('name', 'Super Admin');
        $user = new User;
        $user->username = 'superadmin';
        $user->fullname = 'superadmin';
        $user->email    = 'superadmin@gmail.com';
        $user->password = bcrypt('123123');
        $user->id_groups= $group->id; // as super admin
        $user->save();

        $group = $this->mastergroup->getobject('name', 'Admin');
        $user = new User;
        $user->username = 'admin';
        $user->fullname = 'admin';
        $user->email    = 'admin@gmail.com';
        $user->password = bcrypt('123123');
        $user->id_groups= $group->id; // as peserta
        $user->save();

        $group = $this->mastergroup->getobject('name', 'Peserta');
        $user = new User;
        $user->username = 'peserta';
        $user->fullname = 'peserta';
        $user->telpnumber = '6282130302521';
        $user->email    = 'peserta@gmail.com';
        $user->password = bcrypt('123123');
        $user->id_groups= $group->id; // as peserta
        $user->save();
    }
}
