<?php

use Illuminate\Database\Seeder;
use App\Models\MasterGroup;
use App\Models\MasterMenu;
use App\Models\MapGroupMenu;

class MapGroupMenuSeeder extends Seeder
{
    public $datamenu = [
        [
            'category' => 'A',
            'sort' => '#',
            'icon' => 'user',
            'menuname' => 'Pengguna',
            'urlname' => '/account',
            'routename' => 'account',
        ],
        [
            'category' => 'A',
            'sort' => '2',
            'icon' => 'la la-user-plus',
            'menuname' => 'Pendidikan',
            'urlname' => '/pendidikan',
            'routename' => 'pendidikan',
        ],
        [
            'category' => 'A',
            'sort' => '3',
            'icon' => 'la la-user-plus',
            'menuname' => 'Pekerjaan',
            'urlname' => '/pekerjaan',
            'routename' => 'pekerjaan',
        ],
        [
            'category' => 'A',
            'sort' => '4',
            'icon' => 'la la-user-plus',
            'menuname' => 'Akun Pengguna',
            'urlname' => '/useraccount',
            'routename' => 'useraccount',
        ],
        [
            'category' => 'A',
            'sort' => '5',
            'icon' => 'la la-user-plus',
            'menuname' => 'Akses Pengguna',
            'urlname' => '/userakses',
            'routename' => 'userakses',
        ],
        [
            'category' => 'B',
            'sort' => '#',
            'icon' => 'settings-1',
            'menuname' => 'Pengaturan',
            'urlname' => '/pengaturan',
            'routename' => 'pengaturan',
        ],
        [
            'category' => 'B',
            'sort' => '1',
            'icon' => 'la la-cog',
            'menuname' => 'Artikel',
            'urlname' => '/artikel',
            'routename' => 'artikel',
        ],
        // [
        //     'category' => 'B',
        //     'sort' => '2',
        //     'icon' => 'la la-cog',
        //     'menuname' => 'Kursus',
        //     'urlname' => '/kursus',
        //     'routename' => 'kursus',
        // ],
    ];

    function __construct()
    {
        $this->mastergroup  = new MasterGroup;
        $this->mastermenu  = new MasterMenu;
        $this->mapgroupmenu  = new MapGroupMenu;
    }

    public function run()
    {
        $this->insertmenu();
        // $menuName = in_array('User Group', array_column($this->datamenu, 'menuname'));

        $group = $this->mastergroup->getobject('name', 'Super Admin');
        $this->savemapgroumenu($group);

        // $group = $this->mastergroup->getobject('name', 'Admin');
        // $this->savemapgroumenu($group);
    }

    public function insertmenu(){
        // $this->mastermenu->truncate();
        foreach ($this->datamenu as $key => $value) {
            $menu = $this->mastermenu->storeupdate($value);
            $this->datamenu[$key]['id'] = $menu->id;
        }
    }

    public function savemapgroumenu($group){
        foreach ($this->datamenu as $key => $value) {
            $data = [
                'id_groups' => $group->id,
                'id_menus' => $value['id'],
                'allow_view' => true,
                'allow_create' => true,
                'allow_update' => true,
                'allow_delete' => true,
                'allow_import' => true,
                'allow_export' => true,
                // 'option_view' => true,
                // 'option_create' => true,
                // 'option_update' => true,
                // 'option_delete' => true,
                // 'option_import' => true,
                // 'option_export' => true,
            ];
            $this->mapgroupmenu->storeupdate($data);
        }

        return true;
    }
}
