<?php

use Illuminate\Database\Seeder;
use App\Models\MasterGroup;

class MasterGroupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public $data_basic = [
        [
            'name' => 'Super Admin',
        ],
        [
            'name' => 'Admin'
        ],
        [
            'name' => 'Peserta'
        ],
    ];

    function __construct()
    {
        $this->master  = new MasterGroup;
    }

    public function run(MasterGroup $master)
    {
        foreach ($this->data_basic as $key => $value) {
            $master->storeupdate($value);
        }
    }
}
