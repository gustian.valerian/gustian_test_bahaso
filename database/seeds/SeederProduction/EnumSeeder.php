<?php

use Illuminate\Database\Seeder;
use App\Models\Enum;

class EnumSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public $data = [
        [
            'key' => '1',
            'value' => 'Wanita',
            'category' => 'jenis_kelamin'
        ],
        [
            'key' => '2',
            'value' => 'Pria',
            'category' => 'jenis_kelamin'
        ],
        [
            'key' => '1',
            'value' => 'Menunggu Pembayaran',
            'category' => 'status_order'
        ],
        [
            'key' => '2',
            'value' => 'Terkonfirmasi',
            'category' => 'status_order'
        ],
        [
            'key' => '3',
            'value' => 'Selesai',
            'category' => 'status_order'
        ],
        [
            'key' => '4',
            'value' => 'Batal',
            'category' => 'status_order'
        ],
        [
            'key' => '5',
            'value' => 'Pembayaran Kadaluarsa',
            'category' => 'status_order'
        ],
        [
            'key' => '6',
            'value' => 'Refunded',
            'category' => 'status_order'
        ],
        [
            'key' => '1',
            'value' => 'In Progress',
            'category' => 'status_belajar'
        ],
        [
            'key' => '2',
            'value' => 'Finish',
            'category' => 'status_belajar'
        ],
        [
            'key' => '3',
            'value' => 'Leave',
            'category' => 'status_belajar'
        ],
    ];

    function __construct()
    {
        $this->enum  = new Enum;
    }

    public function run()
    {
        foreach ($this->data as $key => $value){
            $this->enum->storeupdate($value);
        }
    }
}
