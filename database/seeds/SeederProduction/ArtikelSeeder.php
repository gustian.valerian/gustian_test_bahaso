<?php

use Illuminate\Database\Seeder;
use App\Models\Artikel;
use App\Models\ArtikelDetail;

class ArtikelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    function __construct() {
       
        $this->artikel = new Artikel;
        $this->artikel_detail = new ArtikelDetail;
    }
    public function run()
    {
        // $data['artikel'] = 'Office Basic';
        $data['image'] = null;
        $data['title'] = 'Artikel 1';
        $data['description'] = '<b>TEST</b>';

        $perusahaan = $this->artikel->storeupdate($data);

        $data['id_artikel'] = $perusahaan->id;

        $this->artikel_detail->storeupdate($data);
    }
}
