<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;


class CallConfig extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // php artisan vendor:publish --provider="Laravolt\Indonesia\ServiceProvider"

        // php artisan vendor:publish --provider="VXM\Async\AsyncServiceProvider" --tag="config"

        Artisan::call('vendor:publish', [
            '--provider' => 'VXM\Async\AsyncServiceProvider',
            '--tag' => 'config'
        ]);

        Artisan::call('vendor:publish', [
            '--provider' => 'Laravolt\Indonesia\ServiceProvider'
        ]);

        sleep(1);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
