<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArtikelDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('artikel_detail', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('id_artikel')->nullable();
            $table->uuid('id_tag')->nullable();
            $table->timestamps();
        });

        Artisan::call('db:seed', [
            '--class' => 'ArtikelSeeder',
            '--force' => true // <--- add this line
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('artikel_detail');
    }
}
