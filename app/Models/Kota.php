<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Kota extends Model
{
    protected $table = 'cities';
    public $fillable = [ 
        'province_id',
        'name',
        'meta', 
    ];
    public $timestamps = true;

    public function kecamatan(){
        return $this->hasMany('App\Models\Kecamatan', 'city_id', 'id');
    }

    public function provinsi(){
        return $this->belongsTo('App\Models\Provinsi', 'province_id', 'id');
    }
}
