<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuid;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pekerjaan extends Model
{
    use Uuid;

    protected $table = 'pekerjaan';
    public $fillable = [ 
        'pekerjaan'
    ];
    public $incrementing = false;
    public $timestamps = true;

    public function storeupdate($data, $id = null){
        $saveData = $this->firstOrNew(['id' =>  $id]);
        $saveData->pekerjaan          = $data['pekerjaan'] ?? "";
        $saveData->save();
    }
}
