<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuid;
use Illuminate\Database\Eloquent\SoftDeletes;

class ArtikelDetail extends Model
{
    use Uuid;

    protected $table = 'artikel_detail';
    public $fillable = [ 
        'id_artikel'
    ];
    public $incrementing = false;
    public $timestamps = true;

    public function artikel(){
        return $this->hasOne('App\Models\Artikel', 'id_artikel', 'id');
    }

    public function storeupdate($data, $id = null){
        $saveData = $this->firstOrNew([
            'id_artikel' =>  $data['id_artikel']
        ]);
        $saveData->save();
    }
}
