<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Kecamatan extends Model
{
    protected $table = 'districts';
    public $fillable = [ 
        'city_id',
        'name',
        'meta', 
    ];
    public $timestamps = true;

    public function kelurahan(){
        return $this->hasMany('App\Models\Kelurahan', 'district_id', 'id');
    }

    public function kota(){
        return $this->belongsTo('App\Models\Kota', 'city_id', 'id');
    }
}
