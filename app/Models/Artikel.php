<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuid;
use Illuminate\Database\Eloquent\SoftDeletes;

class Artikel extends Model
{
    use Uuid;

    protected $table = 'artikel';
    public $fillable = [ 
        'image',
        'title',
        'description',
    ];
    public $incrementing = false;
    public $timestamps = true;

    public function artikeldetail(){
        return $this->hasOne('App\Models\ArtikelDetail', 'id', 'id_artikel');
    }

    public function getAllData(){
        return $this->get();
    }

    public function storeupdate($data, $id = null){
        $saveData = $this->firstOrNew(['id' =>  $id]);
        $saveData->image          = $data['image'] ?? null;
        $saveData->title          = $data['title'] ?? null;
        $saveData->description    = $data['description'] ?? null;
        $saveData->save();

        return $saveData;
    }
}
