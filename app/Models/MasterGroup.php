<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuid;
use Illuminate\Database\Eloquent\SoftDeletes;

class MasterGroup extends Model
{
    use Uuid;

    protected $table = 'master_groups';
    public $fillable = [ 
        'id',
        'name'
    ];
    public $incrementing = false;
    public $timestamps = true;
 

    public function user(){
        return $this->hasMany('App\Models\User', 'id_groups', 'id');
    }

    public function mapgroupmenu(){
        return $this->hasMany('App\Models\MapGroupMenu', 'id_groups', 'id');
    }

    public function menu(){
        return $this->belongsToMany('App\Models\MasterMenu', 'map_groups_menus', 'id_menus', 'id_groups');
    }

    public function menus()
    {
        return $this->belongsToMany(MasterMenu::class, 'map_groups_menus', 'id_groups', 'id_menus');
    }

    public function storeupdate($data, $id = null){
        $saveData = $this->firstOrNew(['id' =>  $id]);
        $saveData->name          = $data['name'] ?? "";
        $saveData->save();
    }

    public function getobject($key, $value){
        return $this->where($key, '=', $value)->first();
    }

}
