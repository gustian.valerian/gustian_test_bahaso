<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Provinsi extends Model
{
    protected $table = 'province';
    public $fillable = [ 
        'name',
        'meta', 
    ];
    public $timestamps = true;

    public function kota(){
        return $this->hasMany('App\Models\Kota', 'province_id', 'id');
    }
}
