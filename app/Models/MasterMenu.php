<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuid;
use Illuminate\Database\Eloquent\SoftDeletes;

class MasterMenu extends Model
{
    use Uuid;

    protected $table = 'master_menus';
    public $fillable = [ 
        'category',
        'sort', 
        'icon',
        'menuname',
        'urlname',
        'routename',
        'method'
    ];
    public $incrementing = false;
    public $timestamps = true;
 
    public function mapgroupmenu(){
        return $this->hasMany('App\Models\MapGroupMenu', 'id_menus', 'id');
    }

    public function storeupdate($data, $id = null){
        $saveData = $this->firstOrNew(['id' =>  $id]);
        $saveData->category        = $data['category'] ?? "";
        $saveData->sort            = $data['sort'] ?? "";
        $saveData->icon            = $data['icon'] ?? "";
        $saveData->menuname        = $data['menuname'] ?? "";
        $saveData->urlname         = $data['urlname'] ?? "";
        $saveData->routename       = $data['routename'] ?? "";
        $saveData->save();

        return $saveData;
    }
}
