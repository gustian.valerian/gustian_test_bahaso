<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuid;

use Illuminate\Database\Eloquent\SoftDeletes;

class UserRole extends Model
{
    use Uuid;
    protected $table = 'user_role';
    public $fillable = [ 
        'name'
    ];
    public $incrementing = false;
    public $timestamps = true;
 
}
