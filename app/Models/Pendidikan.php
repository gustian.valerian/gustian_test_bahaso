<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuid;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pendidikan extends Model
{
    use Uuid;

    protected $table = 'pendidikan';
    public $fillable = [ 
        'pendidikan'
    ];
    public $incrementing = false;
    public $timestamps = true;

    public function storeupdate($data, $id = null){
        $saveData = $this->firstOrNew(['id' =>  $id]);
        $saveData->pendidikan          = $data['pendidikan'] ?? "";
        $saveData->save();
    }
}
