<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Kelurahan extends Model
{
    protected $table = 'villages';
    public $fillable = [ 
        'district_id',
        'name',
        'meta', 
    ];
    public $timestamps = true;

    public function kecamatan(){
        return $this->belongsTo('App\Models\Kecamatan', 'district_id', 'id');
    }
}
