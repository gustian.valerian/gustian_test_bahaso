<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuid;

use Illuminate\Database\Eloquent\SoftDeletes;

class MapGroupMenu extends Model
{
    use Uuid;

    protected $table = 'map_groups_menus';
    public $fillable = [ 
        'id_groups', 
        'id_menus',
        'key1',
        'key2',
        'value',

        'allow_view',
        'allow_create',
        'allow_update',
        'allow_delete',
        'allow_import',
        'allow_export',
    ];
    public $incrementing = false;
    public $timestamps = true;

    public function group(){
        return $this->belongsTo('App\Models\MasterGroup', 'id_groups', 'id');
    }

    public function menu(){
        return $this->belongsTo('App\Models\MasterMenu', 'id_menus', 'id');
    }

    public function storeupdate($data, $id_groups = null, $id_menus = null){
        $saveData = $this->firstOrNew([
            'id_groups' =>  $data['id_groups'] ?? $id_groups, 
            'id_menus' => $data['id_menus'] ?? $id_menus]);
        $saveData->key1                 = $data['key1'] ?? "";
        $saveData->key2                 = $data['key2'] ?? "";
        $saveData->value                = $data['value'] ?? "";
        $saveData->allow_view           = $data['allow_view'] ?? false;
        $saveData->allow_create         = $data['allow_create'] ?? false;
        $saveData->allow_update         = $data['allow_update'] ?? false;
        $saveData->allow_delete         = $data['allow_delete'] ?? false;
        $saveData->allow_import         = $data['allow_import'] ?? false;
        $saveData->allow_export         = $data['allow_export'] ?? false;
        // $saveData->option_view          = $data['option_view'] ?? false;
        // $saveData->option_create        = $data['option_create'] ?? false;
        // $saveData->option_update        = $data['option_update'] ?? false;
        // $saveData->option_delete        = $data['option_delete'] ?? false;
        // $saveData->option_import        = $data['option_import'] ?? false;
        // $saveData->option_export        = $data['option_export'] ?? false;
        $saveData->save();

        return $saveData;
    }
}
