<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuid;
use Illuminate\Database\Eloquent\SoftDeletes;

class Enum extends Model
{
    use Uuid;

    protected $table = 'enum';
    public $fillable = [ 
        'key',
        'value', 
        'category'
    ];
    public $incrementing = false;
    public $timestamps = true;

    public function storeupdate($data){
        $saveData = $this->firstOrNew([
            'id'    =>  $data['id'] ?? null
        ]);
        $saveData->key          = $data['key'];
        $saveData->value        = $data['value'];
        $saveData->category     = $data['category'];
        $saveData->save();
        return $saveData;
    }

    public function getobject($data){
        $saveData = $this->where('key', $data['key'])
                    ->where('category', $data['category'])
                    ->first();
        return $saveData;
    }

    public function getlistbycat($data){
        $saveData = $this
                    ->where('category', $data)
                    ->get();
        return $saveData;
    }
}
