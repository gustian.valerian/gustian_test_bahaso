<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Passport\HasApiTokens;
use App\Traits\Uuid;
use Illuminate\Support\Str;

class User extends Authenticatable
{
    use Notifiable, HasApiTokens, SoftDeletes;
    use Uuid;

    protected $table = 'master_users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'fullname', 'email', 
        'password', 'telpnumber', 'id_groups'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    public $incrementing = false;

    public function group(){
        return $this->belongsTo('App\Models\MasterGroup', 'id_groups', 'id');
    }

    public function mitrauser(){
        $data =  $this->belongsTo('App\Models\MasterMitra', 'id', 'user_id');
        return $data;
    }

    public function getalluserbygroupname($groupname){
        $data =  $this->whereHas('group', function($q) use($groupname){
            $q->where('name', $groupname);
        })->get();
        return $data;
    }
    
 
    public function storeupdate($data, $id_group, $id_role, $id = null){
        $saveData = $this->firstOrNew(['id' => $id]);

        $saveData->username                    = $data['username'] ?? "";
        $saveData->fullname                    = $data['fullname'] ?? "";
        $saveData->email                       = $data['email'] ?? "";
        if(isset($data['kota_id'])){
            $saveData->kota_id                     = $data['kota_id'] ?? "";
        }
       
        if(isset($data['password'])){
            $saveData->password                     = bcrypt($data['password']);
        }
        $saveData->id_groups                    = $id_group;
        
      
        $saveData->save();

        return $saveData;
    }


    public function getobjectbyid($id){
        $object = $this->find($id);
        return $object;
    }

 
    public function deleteobjectbyid($id){
        $object = $this->destroy($id);
        return $object;
    }

    public function importmitra($data, $id_group, $id_role, $id = null){
        $check_sp_id = MasterMitra::where('sp_id', $data['sp_id'])->first();

        $saveData = $this->withTrashed()->firstOrNew([
            'username' => $data['username'],
            'email'    => $data['email'],
        ]);
        

        $saveData->fullname                    = $data['fullname'] ?? "";
        if(isset($data['kota'])){
            $saveData->kota_id                     = $data['kota'] ?? "";
        }
       
        if(isset($data['password'])){
            $saveData->password                     = bcrypt($data['password']);
        }
        $saveData->id_role                      = $id_role;
        $saveData->id_groups                    = $id_group;
        
        if(isset($saveData->deleted_at)){
            $saveData->restore();
        }
        $saveData->save();
        return $saveData;
    }

}
