<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use App\Models\MapGroupMenu;
use Auth;
use DB;
use Route;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    function permission($menuname){
        if($this->is_api() == true){
            return true;
        }else{

            $menu = MapGroupMenu::where('id_groups', auth()->user()->id_groups)->
            whereHas('menu', function ($q) use ($menuname) { // 
                $q->where('menuname', $menuname);
            })
            ->first();
            return $menu;
        }
    }

    function is_api(){
        $api = str_split(Route::current()->uri, 3)[0];

        if ($api == "api") {
            return true;
        }else{
            return false;
        }

        return false;
    }
}
