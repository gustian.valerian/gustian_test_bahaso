<?php

namespace App\Http\Controllers\Pengguna;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Pendidikan;
use DataTables;
use Auth;
use Illuminate\Support\Str;

class PendidikanController extends Controller
{
    protected $menus;
    protected $permission = 'Pendidikan';

    function __construct() {
        $this->middleware(function ($request, $next) {
            $this->menus = $this->permission($this->permission);
            return $next($request);
        });
        $this->pendidikan = new Pendidikan;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('menu.pengguna.pendidikan.index', ['menu' => $this->menus]);
    }

    
    public function list()
    {
        $user = Auth::user();
        $userGroup = $user->group;
      
        $model = $this->pendidikan->query();

        $dataTables = DataTables::of($model)
                        ->addColumn('show', function ($data) {
                            return route('pendidikan_view_show', ['pendidikan' => $data->id]);
                        })
                        ->addColumn('edit', function ($data) {
                            $url = route('pendidikan_update_edit', ['pendidikan' => $data->id]);
                            return $url;
                        });

        $dataTables = $dataTables->make(true);

        return $dataTables;
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('menu.pengguna.pendidikan.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            //code...
            $data = $request->input();

            $this->pendidikan->storeupdate($data);

            return redirect()->route('pendidikan_view_index')->with('alert-success', $this->permission . ' berhasil di buat');
        } catch (\Throwable $th) {
            return back()->with('alert-failed', $this->permission . ' gagal di buat');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model = $this->pendidikan->where('id', $id)->first();
        return view('menu.pengguna.pendidikan.show', ['model'=>$model]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = $this->pendidikan->where('id', $id)->first();

        return view('menu.pengguna.pendidikan.edit', ['model'=>$model]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            //code...
            $data = $request->input();

            $this->pendidikan->storeupdate($data, $id);

            return redirect()->route('pendidikan_view_index')->with('alert-success', $this->permission . ' berhasil di buat');
        } catch (\Throwable $th) {
            return back()->with('alert-failed', $this->permission . ' gagal di buat');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // dd('a');
        $this->pendidikan->destroy($id);
    }
}
