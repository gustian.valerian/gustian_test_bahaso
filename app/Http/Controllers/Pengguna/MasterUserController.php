<?php

namespace App\Http\Controllers\Pengguna;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\MasterGroup;
use DataTables;
use Auth;
use Illuminate\Support\Str;

class MasterUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $menus;
    protected $permission = 'Akun Pengguna';
    function __construct() {
        $this->middleware(function ($request, $next) {
            $this->menus = $this->permission($this->permission);
            return $next($request);
        });
    }

    public function index()
    {
        return view('menu.pengguna.akunpengguna.index', ['menu' => $this->menus]);
    }

    public function list()
    {
        $user = Auth::user();
        $userGroup = $user->group;
      
        $model = User::query(); //with('group'); //->query();

        $dataTables = DataTables::of($model)
                        ->editColumn('id_groups', function ($data) {
                            return $data->group->name;
                        })
                        ->addColumn('show', function ($data) {
                            return route('useraccount_view_show', ['useraccount' => $data->id]);
                        })
                        ->addColumn('edit', function ($data) {
                            $url = route('useraccount_update_edit', ['useraccount' => $data->id]);
                            return $url;
                        });

        $dataTables = $dataTables->make(true);

        return $dataTables;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $group = MasterGroup::get();
        return view('menu.pengguna.akunpengguna.create', ['group' => $group]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            //code...
            $data = $request->input();
            // dd($data);
            $saveData = new User;
            $saveData->username        = $data['username'];
            $saveData->fullname        = $data['fullname'];//$documentStep->id;
            $saveData->email           = $data['email'];
            $saveData->password        = bcrypt($data['password']);
            $saveData->telpnumber      = $data['telpnumber']; // nullable
            // $saveData->isActive        = $data['isActive']; // nullable
            // $saveData->isVerify        = $data['isVerify'];
            $saveData->id_groups       = $data['group'];

          
            $saveData->save();

            return redirect()->route('useraccount_view_index')->with('alert-success', $this->permission . 'berhasil di buat');
        } catch (\Throwable $th) {
            return back()->with('alert-failed', $this->permission . 'gagal di buat');
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model = User::where('id', $id)->first();
        return view('menu.pengguna.akunpengguna.show', ['model'=>$model]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = User::where('id', $id)->first();
        $group = MasterGroup::get();

        return view('menu.pengguna.akunpengguna.edit', ['model'=>$model, 'group' => $group]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            //code...
            $data = $request->input();
            // dd($data);
            $saveData = User::find($id);
            // dd($saveData);
            $saveData->username        = $data['username'];
            $saveData->fullname        = $data['fullname'];//$documentStep->id;
            $saveData->email           = $data['email'];

            if(isset($data['password'])){
                $saveData->password        = bcrypt($data['password']);
            }
            
            $saveData->telpnumber      = $data['telpnumber']; // nullable
            // $saveData->isActive        = $data['isActive']; // nullable
            // $saveData->isVerify        = $data['isVerify'];
            $saveData->id_groups       = $data['group'];

          
            $saveData->save();

            return redirect()->route('useraccount_view_index')->with('alert-success', $this->permission . 'berhasil di buat');
        } catch (\Throwable $th) {
            return back()->with('alert-failed', $this->permission . 'gagal di buat');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::destroy($id);
    }

    public function import()
    {
        return view('menu.pengguna.akunpengguna.welcome');
    }

    public function export()
    {
        return view('menu.pengguna.akunpengguna.welcome');
    }

    public function profile(){

       $user =  Auth::user();
       $enum = Enum::all();
        if($user->id_role == 1){
           return view('profile.master.edit_admin', compact('user','enum'));
        }elseif ($user->id_role == 2) {
           return view('profile.master.edit_customercare', compact('user','enum'));
        }elseif ($user->id_role == 3) {
           return view('profile.edit_mitra', compact('user','enum'));
        }
    }

}
