<?php

namespace App\Http\Controllers\Pengguna;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Pekerjaan;
use DataTables;
use Auth;
use Illuminate\Support\Str;

class PekerjaanController extends Controller
{
    protected $menus;
    protected $permission = 'Pekerjaan';

    function __construct() {
        $this->middleware(function ($request, $next) {
            $this->menus = $this->permission($this->permission);
            return $next($request);
        });
        $this->pekerjaan = new Pekerjaan;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('menu.pengguna.pekerjaan.index', ['menu' => $this->menus]);
    }

    
    public function list()
    {
        $user = Auth::user();
        $userGroup = $user->group;
      
        $model = $this->pekerjaan->query();

        $dataTables = DataTables::of($model)
                        ->addColumn('show', function ($data) {
                            return route('pekerjaan_view_show', ['pekerjaan' => $data->id]);
                        })
                        ->addColumn('edit', function ($data) {
                            $url = route('pekerjaan_update_edit', ['pekerjaan' => $data->id]);
                            return $url;
                        });

        $dataTables = $dataTables->make(true);

        return $dataTables;
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('menu.pengguna.pekerjaan.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            //code...
            $data = $request->input();

            $this->pekerjaan->storeupdate($data);

            return redirect()->route('pekerjaan_view_index')->with('alert-success', $this->permission . ' berhasil di buat');
        } catch (\Throwable $th) {
            return back()->with('alert-failed', $this->permission . ' gagal di buat');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model = $this->pekerjaan->where('id', $id)->first();
        return view('menu.pengguna.pekerjaan.show', ['model'=>$model]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = $this->pekerjaan->where('id', $id)->first();

        return view('menu.pengguna.pekerjaan.edit', ['model'=>$model]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            //code...
            $data = $request->input();

            $this->pekerjaan->storeupdate($data, $id);

            return redirect()->route('pekerjaan_view_index')->with('alert-success', $this->permission . ' berhasil di buat');
        } catch (\Throwable $th) {
            return back()->with('alert-failed', $this->permission . ' gagal di buat');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // dd('a');
        $this->pekerjaan->destroy($id);
    }
}
