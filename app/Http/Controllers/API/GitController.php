<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Symfony\Component\Process\Process;

class GitController extends Controller
{
    public function gitpull(Request $request){
        // dd('test');
        // dd(env('AUTO_PULL_DIR'));
        try {
            // dd(strtoupper(PHP_OS_FAMILY));
            if(strtoupper(PHP_OS_FAMILY) === "LINUX") {

                $path = '/home/pijatkus/app.pijatku.site';
                $username = $request->user ?? env('AUTO_PULL_USER');
                $password = $request->pass ?? env('AUTO_PULL_PASSWORD');
                // $path = '/Applications/XAMPP/xamppfiles/htdocs/pijatku_web';
                $all = exec("cd '". $path ."/' && git pull https://" . $username .":" . $password ."@gitlab.com/depankomputerdeveloper/pijatku/pijatku_web.git");

                return response()->json($all, 200);
              
            }
        } catch (\Throwable $th) {
            //throw $th;
            return $th;
        }
        
    }
}
