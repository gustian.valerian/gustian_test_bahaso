<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Enum;
use App\Models\MasterKaryawan;
use App\Models\MasterLayanan;
use App\Models\MapMitraLayanan;
use App\Models\MasterMitra;
use Auth;

class ProfileController extends Controller
{
   public function index(MasterMitra $mastermitra,User $user,MasterLayanan $masterlayanan,MasterKaryawan $masterkaryawan){
      $user     =  Auth::user();
      $enum     =  Enum::all();
      $model    = $masterkaryawan->getobjectbyuserid($user->id);
      $userdata = $user->getobjectbyid($user->id);
      $layanan  = MasterLayanan::all();
      if ($user->id_role == 3) {
      $vaksin     =  Enum::where('id', $userdata->usermitra->vaksin)->first();
      $kelamin_mitra     =  Enum::where('id', $userdata->usermitra->jenis_kelamin_mitra)->first();
      $kelamin_customer  =  Enum::where('id', $userdata->usermitra->jenis_kelamin_customer)->first();
   }
      //dd($vaksin);

      if($user->id_role == 1){
         return view('profile.edit_admin', compact('user','enum','model'));
      }elseif ($user->id_role == 2) {
         return view('profile.edit_customercare', compact('user','enum','model'));
      }elseif ($user->id_role == 3) {
         return view('profile.edit_mitra', compact('user','enum','userdata','layanan','vaksin','kelamin_mitra','kelamin_customer'));
      }
   }

   public function profile(MasterMitra $mastermitra,User $user,MasterLayanan $masterlayanan,MasterKaryawan $masterkaryawan){

      
            $user     =  Auth::user();
            $id       = $user->id;
            //dd($id);
            $enum     = Enum::all();
            $model    = $masterkaryawan->getobjectbyuserid($id);
            //dd($model);
            $userdata          = $user->getobjectbyid($id);
            $layanan           = MasterLayanan::all();
            if ($user->id_role == 3) {
               # code...
               $vaksin            =  Enum::where('id', $userdata->usermitra->vaksin)->first();
               $kelamin_mitra     =  Enum::where('id', $userdata->usermitra->jenis_kelamin_mitra)->first();
               $kelamin_customer  =  $userdata->usermitra->jenis_kelamin_customer == 0 ? 'Laki-laki & Wanita' : Enum::where('id', $userdata->usermitra->jenis_kelamin_customer)->first()->value;
            }
   
            // dd($kelamin_customer);
         if($user->id_role == 1){
            return view('profile.edit_admin', compact('user','enum','model'));
         }elseif ($user->id_role == 2) {
            return view('profile.edit_customercare', compact('user','enum','model'));
         }elseif ($user->id_role == 3) {
            return view('profile.edit_mitra', compact('user','enum','userdata','layanan','vaksin','kelamin_mitra','kelamin_customer'));
         }
     }

     public function admin_update(MasterKaryawan $masterkaryawan, User $user, Request $request){
          // dd($id);
          $data = $request->input();
          // dd($data);
          $id     =  Auth::user()->id;
          $groups_id     =  Auth::user()->id_groups;

          //cari data gambar lama
          $path_photolama = $masterkaryawan->getobjectbyuserid($id);
          $path_photo = $request->file('path_photo');
          
          
          if(!empty($path_photo)){
             if(!empty($path_photolama['path_photo'])){
               $photo = 'public/photokaryawan/'.$path_photolama['path_photo'];
               Storage::disk('local')->delete($photo);
            }
            
            $path_photo->storeAs('public/photokaryawan/', $path_photo->hashName());

            $data['path_photo'] = $path_photo->hashName();
         }

          $id_role  = 1;

          if($groups_id == 1){
             $id_group = 1; 
          }else{
             $id_group = 2; 
          }
  
          $karyawan = $user->storeupdate($data, $id_group, $id_role, $id);
          $masterkaryawan->storeupdate($data, $karyawan->id);
  
          return redirect()->route('view_profile')->with('alert-success', 'Update Admin Berhasil');
     }

     public function customercare_update(MasterKaryawan $masterkaryawan, User $user, Request $request){
        
         $data = $request->input();
         $id     =  Auth::user()->id;

         
         $path_photolama = $masterkaryawan->getobjectbyuserid($id);
         $path_photo = $request->file('path_photo');
         
         if(!empty($path_photo)){
            $photo = 'public/photokaryawan/'.$path_photolama['path_photo'];
            if(!empty($path_photolama['path_photo'])){
               Storage::disk('local')->delete($photo);
            }
            
            $path_photo->storeAs('public/photokaryawan/', $path_photo->hashName());

            $data['path_photo'] = $path_photo->hashName();
         }

         $id_role = 2;
         $id_group = 3; 

         $karyawan = $user->storeupdate($data, $id_group, $id_role, $id);
         $masterkaryawan->storeupdate($data, $karyawan->id);

         return redirect()->route('view_profile')->with('alert-success', 'Update Customer Care Berhasil');
     }

     public function mitra_update(MasterMitra $mastermitra,MapMitraLayanan $mapmitralayanan,User $user,MasterLayanan $masterlayanan,Request $request){
      $data = $request->input();
      $id     =  Auth::user()->id;

     
      $path_photolama = MasterMitra::where('user_id', $id)->first();
      $path_photo = $request->file('path_photo');

              if($path_photo != ''){
                  $photo = 'public/photomitra/'.$path_photolama['foto_mitra'];
                  if(!empty($path_photolama['path_photo'])){
                      Storage::disk('local')->delete($photo);
                  }
                  
                  $path_photo->storeAs('public/photomitra/', $path_photo->hashName());

                  $data['path_photo'] = $path_photo->hashName();
              }else{
                  $data['path_photo'] = $path_photolama['foto_mitra'];
              }

              $id_role = 3;
              $id_group = 4;

              $masterkaryawan = $user->storeupdate($data,$id_group, $id_role, $id);
              $master = $mastermitra->storeupdate($data, $masterkaryawan->id);

              return redirect()->route('view_profile')->with('alert-success', 'Update Mitra Berhasil');
     }
}
