<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
class RegisterController extends Controller
{
    public function showRegistrationForm(){
        return view('auth.register');
    }
    
    protected function register(Request $request)
    {

        try {
            //code...
            User::create([
                'username' => $request->username,
                'email' => $request->email,
                'password' => Hash::make($request->password),
            ]);
    
            return redirect('login')->with('alert', 'Pendaftaran berhasil, menunggu konfirmasi admin');
        } catch (\Throwable $th) {
            return back()->with('alert-failed', 'Pendaftaran gagal di buat');
        }
    }
}
