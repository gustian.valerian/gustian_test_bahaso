<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;
use Route;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers {
        logout as performLogout;
    }

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function logout(Request $request)
    {
        $this->performLogout($request);
        return redirect()->route('login');
    }

    /**
     * @OA\Post(
     * path="/api/login",
     * summary="Sign in",
     * description="Login by email, password",
     * operationId="authLogin",
     * tags={"auth"},
     * @OA\RequestBody(
     *    required=true,
     *    description="Pass user credentials",
     *    @OA\JsonContent(
     *       required={"email","password"},
     *       @OA\Property(property="email", type="string", format="email", example="admin"),
     *       @OA\Property(property="password", type="string", format="password", example="123123"),
     *       @OA\Property(property="persistent", type="boolean", example="true"),
     *    ),
     * ),
     * @OA\Response(
     *    response=422,
     *    description="Wrong credentials response",
     *    @OA\JsonContent(
     *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
     *        )
     *     )
     * )
     */

    public function login(Request $request)
    {   
        try {
        $input = $request->all();

        $api = str_split(Route::current()->uri, 3)[0];

        $validate = [
            'email' => 'required',
            'password' => 'required',
        ];
            
        if ($api != "api") {
            array_push($validate, ['_answer' => 'required|simple_captcha']);
        }

        $this->validate($request, $validate);


        
        $fieldType = filter_var($request->email, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';

        if(auth()->attempt(array($fieldType => $input['email'], 'password' => $input['password'])))
        {
            // dd('test');
            // return response()->json(['admin_sucsess']);
            if (\Route::current()->getName() == 'LoginApi') {
                if ($api == 'api') {

                    // return Auth::user()->group;
                    // return Auth::user()->group->mapgroupmenu->toJson();
                    Auth::user()->group->menus;
                    $token = auth()->user()->createToken('bahaso')->accessToken;
                    // $token = Auth::user()->createToken('bahaso')->accessToken;

                    return response()->json([
                        'token' => $token,
                        // 'user'=> auth()->user()
                    ], 200);
                }
            }
            return redirect()->route('dashboard');
        }else{
            return redirect()->route('login')
                ->with('error','Email-Address And Password Are Wrong.');
        }
        } catch (\Throwable $th) {
            return response()->json(['error'=> $th], 404);
        }
        
    }
}
