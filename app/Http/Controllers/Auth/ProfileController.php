<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\Models\Artikel;
use App\Models\User;
use App\Models\Enum;
use DataTables;

class ProfileController extends Controller
{

    function __construct() {
        $this->artikel = new Artikel;
        $this->user = new User;
        $this->enum = new Enum;

    }

    public function getSertifikat()
    {
        return view('auth.profile.sertifikat.index');
    }

    public function getSertifikatList()
    {
        $user = Auth::user();
        $userGroup = $user->group;
        
        $model = $this->kursus->with(['artikel', 'peserta', 'statuscourse'])
                ->where('peserta_id', Auth::user()->id)
                ->get();

        
        $dataTables = DataTables::of($model)
        ->addColumn('sertifikat', function ($data) {
            return route('profile_view_sertifikatobject', ['id' => $data->id]);
        });

        $dataTables = $dataTables->make(true);

        return $dataTables;
    }

    public function getSertifikatObject($id){
        // dd($id);
        $model = $this->kursus->where('id', $id)->where('peserta_id', Auth::user()->id)->with(['artikel', 'peserta'])->first();
        return view('menu.pengaturan.kursus.sertifikat', ['model'=>$model]);
    }

    public function getSertifikatObjectCeck($id){
        // dd($id);
        $model = $this->kursus->where('id', $id)->with(['artikel', 'peserta'])->first();
        return view('menu.pengaturan.kursus.singlesertifikat', ['model'=>$model]);
    }
}
