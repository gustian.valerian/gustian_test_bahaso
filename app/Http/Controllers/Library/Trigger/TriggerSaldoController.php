<?php

namespace App\Http\Controllers\Library\Trigger;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\MasterMitra;

class TriggerSaldoController extends Controller
{
    function __construct()
    {
        $this->saldototal = new MasterMitra;
    }

    public function triggerInsertSaldo($mitraId, $nominal ){
        //dd($nominal, $mitraId);
        $this->saldototal->add_saldomitra($mitraId, $nominal);
    }

    public function triggerUpdateSaldo($mitraId, $nominallama, $nominalbaru ){
        //dd($mitraId, $nominallama, $nominalbaru );
        $this->saldototal->update_saldomitra($mitraId, $nominallama, $nominalbaru);
    }

    public function triggerDeleteSaldo($mitraId, $nominal ){
        //dd($mitraId, $nominallama, $nominalbaru );
        $this->saldototal->delete_saldomitra($mitraId, $nominal);
        return true;
    }

    public function triggerRestoreSaldo($mitraId, $nominal){
        //dd($mitraId, $nominallama, $nominalbaru );
        $this->saldototal->restore_saldomitra($mitraId, $nominal);
    }
}
