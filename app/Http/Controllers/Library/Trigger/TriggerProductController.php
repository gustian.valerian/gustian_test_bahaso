<?php

namespace App\Http\Controllers\Library\Trigger;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\MasterMitraStock;
use App\Models\TransaksiProdukDetail;

class TriggerProductController extends Controller
{
    function __construct()
    {
        $this->masterMitraStock = new MasterMitraStock;
    }

    public function triggerInsert($productId, $qtyProduct, $mitraId){
        $this->masterMitraStock->add_stock($productId, $qtyProduct, $mitraId);
    }

    public function triggerUpdate($productId, $qtyProduct, $mitraId){
        $this->masterMitraStock->add_stock($productId, $qtyProduct, $mitraId);
    }

    public function triggerDelete($productId, $qtyProduct, $mitraId){
        // dd($productId, $qtyProduct, $mitraId);
        $this->masterMitraStock->reduce_stok($productId, $qtyProduct, $mitraId);
    }
}
