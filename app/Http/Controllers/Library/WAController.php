<?php

namespace App\Http\Controllers\Library;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use App\Jobs\SendWAJob;

class WAController extends Controller
{
    public $base_url_wav3;
    public $tokenwav3;

    public $base_url_wav4;
    public $tokenwav4;
    public $sender;

    function __construct()
    {
        // $this->base_url  = "http://207.148.78.159:8000/wa-v4/api";
        // $this->tokenwav3     = "c8dfe29e3a0946519cceb81b5f7e001f8f8f72ba";
        $this->base_url_wav3  = "http://207.148.78.159:8000/wav3/api";
        $this->tokenwav3     = "gustianvalerian";
        

        $this->base_url_wav4  = "http://207.148.78.159:8000/wa-v4/api";
        // $this->base_url_wav4  = "http://165.232.171.180/wa-v4/api";
        $this->tokenwav4     = "c8dfe29e3a0946519cceb81b5f7e001f8f8f72ba";
        // $this->sender    = "6285695307080";
        $this->sender    = "6287873037000";

        $this->base_url_wav44  = "http://159.223.61.62:8000";
        $this->sender44    = "6287873037000";

        $this->base_url_v4  = "http://159.223.61.62:81/wa-v4/api";
        $this->tokenv4     = "a52419afe06e100b2f5df4d6e95e1f5338cac667";
        $this->senderv4    = "6287873037000";
    }

    public function getSendMessageV3($number = null, $message = null){
        // https://localhost/pijatku_web/public/api/send_message/087873434070/test%20gustian
        $response = Http::post( $this->base_url_wav3 . '/send-message.php',
            [ 
                "api_key" => $this->tokenwav3,
                // "sender" => $this->sender,
                "number" => $number,
                "message" => $message
            ])->json();

        return response()->json($response);
    }

    public function getSendMessageV4($number = null, $message = null, $isCustomer = false){
        // dd($number);
        // if($isCustomer){
        //     $message = $message . "\n----- Balas ke https://bit.ly/pijatku_customer -----";
        // }else{
        //     $message = $message . "\n----- Balas ke https://bit.ly/pijatku_mitra -----";
        // }

        // $response = Http::post( $this->base_url_v4 . '/send-message.php',
        // [ 
        //     "api_key" => $this->tokenv4,
        //     "sender" => $this->senderv4,
        //     "number" => $number,
        //     "message" => $message
        // ])->json();
        
        // return $response;
        
        $sendWAJob = SendWAJob::dispatch($number, $message)->delay(2); // seconds
            // dd($message);
        // $response = Http::post( $this->base_url_wav44 . "/send-message",
        // [ 
        //     "number" => $number,
        //     "message" => $message
        // ]);

        // return $response;

        // dd($message);
        // https://localhost/pijatku_web/public/api/send_message/087873434070/test%20gustian
        // $response = Http::post( $this->base_url_wav4 . '/send-message.php',
        //     [ 
        //         "api_key" => $this->tokenwav4,
        //         "sender" => $this->sender,
        //         "number" => $number,
        //         "message" => $message
        //     ])->json();
        // return response()->json($response);
        // exec("/usr/local/bin/php /home/pijatkus/app.pijatku.site/artisan queue:work --once >> /dev/null 2>&1");
        // Artisan::call('queue:work --once');

        // dispatch(new \App\Jobs\SendWAJob($number, $message));
        // $sendWAJob = (new SendWAJob($number, $message)); //->onQueue('wa-jobs');
        // // dd($sendWAJob);
        // $this->dispatch($sendWAJob);

        // or if you want to delay it

        // $sendWAJob = (new SendWAJob($number, $message))->delay(30); // seconds
        // $response = Http::post( "http://165.232.171.180:8000" . '/send-message.php',
        // [ 
        //     // "api_key" => $this->tokenwav4,
        //     // "sender" => $this->sender,
        //     "number" => (string)$number,
        //     "message" => (string)$message
        // ]);
    }
}
