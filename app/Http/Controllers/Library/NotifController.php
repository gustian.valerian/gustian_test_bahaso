<?php

namespace App\Http\Controllers\Library;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\Library\WAController;

class NotifController extends Controller
{
    public function __construct()
    {
        $this->WA = new WAController;
    }

    public function notifProduk($data) {
        $message = 
        'UPDATE PRODUK PIJATKU

        Nama Mitra : '.$data['fullname'].'
        SP ID : '.$data['invoice'].'
        Kota : '.$data['kota'].'
        Tanggal Pembelian : '.$data['tanggal'].'
        Pembelian Produk : '.$data['produk'].'
        Potongan Harga : Rp. '.number_format($data['potongan']).'
        Ongkos Kirim : Rp. '.number_format($data['ongkir']).'
        Total Pembelian : Rp '.number_format($data['total']).' 
        Jenis Pembayaran : '.$data['payment'].' 
        Keterangan : '.$data['status'].'

        Mohon agar tidak membalas di No WA ini karena ini adalah WA AUTO GENERATED 

        Jika ada pertanyaan, silahkan menghubungi NO WA ADMIN Pijatku 

        Terima kasih, Salam sehat selalu 🙏🏻';
    
        $this->WA->getSendMessageV4($data['no_wa'], str_replace('        ', '', $message));
    }

    public function notifSaldoMitra($data) 
    {
        $message = 
        'TOP UP SALDO PIJATKU

        Nama Mitra : '.$data['fullname'].'
        SP ID : '.$data['invoice'].'
        Kota : '.$data['kota'].'
        Tanggal Pembelian : '.$data['tanggal'].'
        Saldo Awal : Rp '.number_format($data['saldoawal']).'
        Top Up Saldo : Rp '.number_format($data['topup']).' 
        Saldo Terbaru : Rp '.number_format($data['saldobaru']).' 
        Keterangan : '.$data['catatan'].'

        Mohon agar tidak membalas di No WA ini karena ini adalah WA AUTO GENERATED 

        Jika ada pertanyaan, silahkan menghubungi NO WA ADMIN Pijatku 

        Terima kasih, Salam sehat selalu 🙏🏻';
    
        $this->WA->getSendMessageV4($data['no_wa'], str_replace('        ', '', $message));
    }

    public function notifMitraGetOrder($data)
    {
        // $message = '';
        // $message.='
        // GET ORDER
        // Invoice : '.$data["invoice"].'
        // Nama Pemesan  :'.$data["namapemesan"].'
        // Gender : '.$data["kelamincustomer"].'
        // Alamat  : '.$data["alamatcustomer"].'
   
        // No WA  : '.$data["wacustomer"].'

        // Pemesanan Untuk : '.$data["pemesananuntuk"].'

        // *Jika untuk orang lain maka keluar nama yg dipesankan, alamat lengkap dan no wa)

        // Layanan Yg Dipesan : '.$data["layanandipesan"].'
        // Durasi Layanan : '.$data["durasilayanan"].'
        // Tanggal Pemesanan : '.$data["tanggalpemesanan"].'
        // Jam Pemesanan :  '.$data["jampemesanan"].'
        // Tanggal Layanan : '.$data["tanggallayanan"].'
        // Jam Layanan :  '.$data["jamlayanan"].'
        // Harga Layanan :  '.$data["hargalayanan"].'
        // Jenis Pembayaran : '.$data["jenispembayaran"].'

        // *jika ada add on

        // Penambahan Layanan : '.$data["penambahanlayanan"].'
        // Add On Service : '.$data["addonservice"].'
        // Harga Layanan : Rp '.$data["hargalayanan2"].' 
        // Jenis Pembayaran Add-Ons : '.$data["jenispembayaranaon"].'

        // Pembayaran Tunai Yang Akan Diterima Dari Customer :

        // Mitra  : 

        // Potongan/Penambahan Saldo Setelah Layanan Selesai :

        // Mohon agar tidak membalas di No WA ini karena ini adalah WA AUTO GENERATED

        // Jika ada pertanyaan silahkan menghubungi NO WA CALL CENTER Pijatku

        // Terima kasih 🙏🏻';

        if($data['jenisbayar'] == '17'){ // cash
            $plusminuspotongan = '-';
        }else{
            $plusminuspotongan = '+';
        }

        $message = '';
        $message.='
        GET ORDER
        Invoice : '.$data["invoice"].'
        Nama Pemesan : '.$data["namapemesan"].'
        Gender : '.$data["kelamincustomer"].'
        Alamat : '.$data["alamatcustomer"].'
   
        No WA : '.$data["wacustomer"].'
        '.$data["pemesananuntuk"].'
        '.$data['layanandipesan'].'
        '.$data['layananaddons'].'
        '.$data['pembayaranifcash'].'

        Mitra : '.$data['namamitra'].'

        Potongan/Penambahan Saldo Setelah Layanan Selesai : '.$plusminuspotongan.' Rp. '.number_format($data['saldomitra']).'

        Mohon agar tidak membalas di No WA ini karena ini adalah WA AUTO GENERATED

        Jika ada pertanyaan silahkan menghubungi NO WA CALL CENTER Pijatku

        Terima kasih 🙏🏻';

        $this->WA->getSendMessageV4($data['no_wa'], str_replace('        ', '', $message));

    }

    public function notifReminder($data)
    {
        $message = '';

        $message.='
        REMINDER

        MOHON PERHATIANNYA :

        - Segera lakukan konfirmasi pemesanan 
        - Selalu jalankan prokes memakai masker dan face shield, mencuci tangan dan seragam Pijatku
        - Tidak terlambat dan selalu dalam keadaan rapi dan juga bersih
        - Peralatan dan perlengkapan juga dipastikan selalu lengkap dan bersih
        - Membawa produk sesuai pemesanan
        - Pastikan sudah menerima pembayaran sebelum meninggalkan lokasi
        - Pastikan tidak ada barang tertinggal atau barang customer yang terbawa

        Mohon agar tidak membalas di No WA ini karena ini NO WA AUTO GENERATED

        Jika ada pertanyaan atau ada perubahan /  penambahan / pengurangan layanan / cancel, silahkan menghubungi NO WA CALL CENTER Pijatku

        Hati-hati di perjalanan, terima kasih 🙏🏻';

        $this->WA->getSendMessageV4($data['no_wa'], str_replace('        ', '', $message));
    }

    public function notifFinishOrdered($data)
    {   
        $message = '';
        // $message.='
        // UPDATE SALDO MITRA PIJATKU

        // Nama Mitra :  '.$data['namamitra'].'
        // SP ID :  '.$data['spid'].'
        // Kota :  '.$data[''].'
        // No Pemesanan : '.$data[''].'
        // Tanggal Pemesanan :  '.$data[''].'
        // Tanggal Layanan :  '.$data[''].'
        // Nama Customer :  '.$data[''].'
        // Layanan :  '.$data[''].'
        // Durasi :  '.$data[''].'
        // Harga Layanan : Rp '.number_format($data['']).' 
        // Jenis Pembayaran :  '.$data[''].'
        // Saldo Awal : Rp '.number_format($data['']).' 
        // Penambahan/Pengurangan Saldo : Rp '.number_format($data['']).' 
        // Saldo Terbaru : Rp '.number_format($data['']).' 
        // Keterangan :  '.$data[''].'

        // Mohon agar tidak membalas di No WA ini karena ini NO WA AUTO GENERATED

        // Jika ada pertanyaan, silahkan hubungi NO WA ADMIN Pijatku

        // Terima kasih 🙏🏻';
        
        if($data['jenisbayar'] == 'Cash'){ //+
            $hitungsaldo = $data['saldoawalmitra']-$data['saldomitra'];
            $plusminsaldo = '-';
        }else{ // -
            $hitungsaldo = $data['saldoawalmitra']+$data['saldomitra'];
            $plusminsaldo = '+';
        }

        // if($data['diskon'] > 0){
        //     $valdiskon = '';
        // }

        $message.='
        UPDATE SALDO MITRA PIJATKU

        Nama Mitra : '.$data['namamitra'].'
        SP ID : '.$data['spid'].'
     
        No Pemesanan : '.$data['invoice'].'
        Nama Pemesan : '.$data["namapemesan"].'
        Gender : '.$data["kelamincustomer"].'
        Alamat : '.$data["alamatcustomer"].'
        
        '.$data["pemesananuntuk"].'
        '.$data['layanandipesan'].'
        '.$data['layananaddons'].'

        Saldo Awal : Rp '.number_format($data['saldoawalmitra']).' 
        Penambahan/Pengurangan Saldo : '.$plusminsaldo.' Rp '.number_format($data['saldomitra']).' 
        Saldo Terbaru : Rp '.number_format($hitungsaldo).' 

        Mohon agar tidak membalas di No WA ini karena ini NO WA AUTO GENERATED

        Jika ada pertanyaan, silahkan hubungi NO WA ADMIN Pijatku

        Terima kasih 🙏🏻';
    
        $this->WA->getSendMessageV4($data['no_wa'], str_replace('        ', '', $message));
    }

    public function notifCustomer($data)
    {
        $message = '';
        $message.='
        Selamat Bapak/Ibu/Saudara/i '.$data['fullname'].' anda mendapatkan Voucher Potongan Harga sebesar 10%
        ';

        $this->WA->getSendMessageV4($data['no_wa'], str_replace('        ', '', $message));
    }
}
