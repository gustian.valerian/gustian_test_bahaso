<?php

namespace App\Http\Controllers\Pengaturan;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Artikel;
use App\Models\ArtikelDetail;
use DataTables;
use Auth;
use Illuminate\Support\Str;

class ArtikelController extends Controller
{
    protected $menus;
    protected $permission = 'Artikel';

    function __construct() {
        $this->middleware(function ($request, $next) {
            $this->menus = $this->permission($this->permission);
            return $next($request);
        });
        $this->artikel = new Artikel;
        $this->artikel_detail = new ArtikelDetail;
        $this->apis = $this->is_api();

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('menu.pengaturan.artikel.index', ['menu' => $this->menus]);
    }

    
    public function list()
    {      
        // dd('asd');
        // return response()->json(auth()->user());
        $model = $this->artikel->query();
        // dd($model);
        $dataTables = DataTables::of($model)
                        ->addColumn('show', function ($data) {
                            return route('artikel_view_show', ['artikel' => $data->id]);
                        })
                        ->addColumn('edit', function ($data) {
                            $url = route('artikel_update_edit', ['artikel' => $data->id]);
                            return $url;
                        });

        $dataTables = $dataTables->make(true);

        return $dataTables;
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('menu.pengaturan.artikel.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            //code...
            $data = $request->input();
            // dd($request->all());
            $perusahaan = $this->artikel->storeupdate($data);

            $request->request->add(['instansi_id' =>  $perusahaan->id ]);
            $data['id_artikel'] = $perusahaan->id;

            $d = $this->artikel_detail->storeupdate($data);

            if($this->apis == true){
                return response()->json(
                   [ "message" => $this->permission . ' berhasil di buat',
                   "data"=> $perusahaan]
                , 200);
            }else{
                return redirect()->route('artikel_view_index')->with('alert-success', $this->permission . ' berhasil di buat');
            }
        } catch (\Throwable $th) {
            return back()->with('alert-failed', $this->permission . ' gagal di buat');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model = $this->artikel->where('id', $id)->first();
        if($this->apis == true){
            return response()->json(
               [ "message" => $this->permission . ' berhasil di tampilkan',
               "data"=> $model]
            , 200);
        }else{
            return view('menu.pengaturan.artikel.show', ['model'=>$model]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = $this->artikel->where('id', $id)->first();

        return view('menu.pengaturan.artikel.edit', ['model'=>$model]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            //code...
            $data = $request->input();

            $perusahaan = $this->artikel->storeupdate($data, $id);

            $data['id_artikel'] = $perusahaan->id;

            $this->artikel_detail->storeupdate($data);
            if($this->apis == true){
                return response()->json(
                   [ "message" => $this->permission . ' berhasil di ubah',
                   "data"=> $perusahaan]
                , 200);
            }else{
                return redirect()->route('artikel_view_index')->with('alert-success', $this->permission . ' berhasil di ubah');
            }
        } catch (\Throwable $th) {
            return back()->with('alert-failed', $this->permission . ' gagal di buat');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // dd('a');
        $temp = $this->artikel->find($id);

        $this->artikel->destroy($id);

        if($this->apis == true){
            return response()->json(
               [ "message" => $this->permission . ' berhasil di hapus',
               "data"=> $temp]
            , 200);
        }
    }
}
