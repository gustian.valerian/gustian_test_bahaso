<div class="modal-body">
    <form>
      <div class="mb-3">
        <label class="form-label">Name</label>
        <input name="Name" type="text" class="form-control" />
      </div>
      <div class="mb-3">
        <label class="form-label">Sales</label>
        <input name="Sales" type="number" class="form-control" />
      </div>
      <div class="mb-3">
        <label class="form-label">Stock</label>
        <input name="Stock" type="number" class="form-control" />
      </div>
      <div class="mb-3">
        <label class="form-label">Category</label>
        <div class="form-check">
          <input type="radio" id="category1" name="Category" value="Whole Wheat" class="form-check-input" />
          <label class="form-check-label" for="category1">Whole Wheat</label>
        </div>
        <div class="form-check">
          <input type="radio" id="category2" name="Category" value="Sourdough" class="form-check-input" />
          <label class="form-check-label" for="category2">Sourdough</label>
        </div>
        <div class="form-check">
          <input type="radio" id="category3" name="Category" value="Multigrain" class="form-check-input" />
          <label class="form-check-label" for="category3">Multigrain</label>
        </div>
      </div>
      <div class="mb-3">
        <label class="form-label">Tag</label>
        <div class="form-check">
          <input type="radio" id="tag1" name="Tag" value="New" class="form-check-input" />
          <label class="form-check-label" for="tag1">New</label>
        </div>
        <div class="form-check">
          <input type="radio" id="tag2" name="Tag" value="Sale" class="form-check-input" />
          <label class="form-check-label" for="tag2">Sale</label>
        </div>
        <div class="form-check">
          <input type="radio" id="tag3" name="Tag" value="Done" class="form-check-input" />
          <label class="form-check-label" for="tag3">Done</label>
        </div>
      </div>
    </form>
  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-outline-primary" data-bs-dismiss="modal">Cancel</button>
    <button type="button" class="btn btn-primary" id="addEditConfirmButton">Add</button>
  </div>