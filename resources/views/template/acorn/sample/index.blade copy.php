@extends('template.acorn')

@section('title',' Customer')

@section('css')
<link rel="stylesheet" href="{{ asset('acorn/css/vendor/datatables.min.css') }}" />

@endsection

@section('breadcrumbs')
    <!-- Title and Top Buttons Start -->
    <div class="page-title-container">
        <div class="row">
          <!-- Title Start -->
          <div class="col-12 col-md-7">
            <h1 class="mb-0 pb-0 display-4" id="title">Server Side Rows Datatables</h1>
            <nav class="breadcrumb-container d-inline-block" aria-label="breadcrumb">
              <ul class="breadcrumb pt-0">
                <li class="breadcrumb-item"><a href="Dashboards.Default.html">Home</a></li>
                <li class="breadcrumb-item"><a href="Interface.html">Interface</a></li>
                <li class="breadcrumb-item"><a href="Interface.Plugins.html">Plugins</a></li>
                <li class="breadcrumb-item"><a href="Interface.Plugins.Datatables.html">Datatables</a></li>
              </ul>
            </nav>
          </div>
          <!-- Title End -->

          <!-- Top Buttons Start -->
          <div class="col-12 col-md-5 d-flex align-items-start justify-content-end">
            <!-- Add New Button Start -->
            <button type="button" class="btn btn-outline-primary btn-icon btn-icon-start w-100 w-md-auto add-datatable">
              <i data-cs-icon="plus"></i>
              <span>Add New</span>
            </button>
            <!-- Add New Button End -->

            {{-- <!-- Check Button Start -->
            <div class="btn-group ms-1 check-all-container">
              <div class="btn btn-outline-primary btn-custom-control p-0 ps-3 pe-2" id="datatableCheckAllButton">
                <span class="form-check float-end">
                  <input type="checkbox" class="form-check-input" id="datatableCheckAll" />
                </span>
              </div>
              <button
                type="button"
                class="btn btn-outline-primary dropdown-toggle dropdown-toggle-split"
                data-bs-offset="0,3"
                data-bs-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
                data-submenu
              ></button>
              <div class="dropdown-menu dropdown-menu-end">
                <div class="dropdown dropstart dropdown-submenu">
                  <button class="dropdown-item dropdown-toggle tag-datatable caret-absolute disabled" type="button">Tag</button>
                  <div class="dropdown-menu">
                    <button class="dropdown-item tag-done" type="button">Done</button>
                    <button class="dropdown-item tag-new" type="button">New</button>
                    <button class="dropdown-item tag-sale" type="button">Sale</button>
                  </div>
                </div>
                <div class="dropdown-divider"></div>
                <button class="dropdown-item disabled delete-datatable" type="button">Delete</button>
              </div>
            </div>
            <!-- Check Button End --> --}}
          </div>
          <!-- Top Buttons End -->
        </div>
    </div>
    <!-- Title and Top Buttons End -->
@endsection

@section('content')
    <!-- Content Start -->
    <div class="data-table-rows slim">
        {{-- <!-- Controls Start -->
        <div class="row">
          <!-- Search Start -->
          <div class="col-sm-12 col-md-5 col-lg-3 col-xxl-2 mb-1">
            <div class="d-inline-block float-md-start me-1 mb-1 search-input-container w-100 shadow bg-foreground">
              <input class="form-control datatable-search" placeholder="Search" data-datatable="#datatableRowsServerSide" />
              <span class="search-magnifier-icon">
                <i data-cs-icon="search"></i>
              </span>
              <span class="search-delete-icon d-none">
                <i data-cs-icon="close"></i>
              </span>
            </div>
          </div>
          <!-- Search End -->

          <div class="col-sm-12 col-md-7 col-lg-9 col-xxl-10 text-end mb-1">
            <div class="d-inline-block me-0 me-sm-3 float-start float-md-none">
              <!-- Add Button Start -->
              <button
                class="btn btn-icon btn-icon-only btn-foreground-alternate shadow add-datatable"
                data-bs-delay="0"
                data-bs-toggle="tooltip"
                data-bs-placement="top"
                title="Add"
                type="button"
              >
                <i data-cs-icon="plus"></i>
              </button>
              <!-- Add Button End -->

              <!-- Edit Button Start -->
              <button
                class="btn btn-icon btn-icon-only btn-foreground-alternate shadow edit-datatable disabled"
                data-bs-delay="0"
                data-bs-toggle="tooltip"
                data-bs-placement="top"
                title="Edit"
                type="button"
              >
                <i data-cs-icon="edit"></i>
              </button>
              <!-- Edit Button End -->

              <!-- Delete Button Start -->
              <button
                class="btn btn-icon btn-icon-only btn-foreground-alternate shadow disabled delete-datatable"
                data-bs-delay="0"
                data-bs-toggle="tooltip"
                data-bs-placement="top"
                title="Delete"
                type="button"
              >
                <i data-cs-icon="bin"></i>
              </button>
              <!-- Delete Button End -->
            </div>
            <div class="d-inline-block">
              <!-- Print Button Start -->
              <button
                class="btn btn-icon btn-icon-only btn-foreground-alternate shadow datatable-print"
                data-bs-delay="0"
                data-datatable="#datatableRowsServerSide"
                data-bs-toggle="tooltip"
                data-bs-placement="top"
                title="Print"
                type="button"
              >
                <i data-cs-icon="print"></i>
              </button>
              <!-- Print Button End -->

              <!-- Export Dropdown Start -->
              <div class="d-inline-block datatable-export" data-datatable="#datatableRowsServerSide">
                <button class="btn p-0" data-bs-toggle="dropdown" type="button" data-bs-offset="0,3">
                  <span
                    class="btn btn-icon btn-icon-only btn-foreground-alternate shadow dropdown"
                    data-bs-delay="0"
                    data-bs-placement="top"
                    data-bs-toggle="tooltip"
                    title="Export"
                  >
                    <i data-cs-icon="download"></i>
                  </span>
                </button>
                <div class="dropdown-menu shadow dropdown-menu-end">
                  <button class="dropdown-item export-copy" type="button">Copy</button>
                  <button class="dropdown-item export-excel" type="button">Excel</button>
                  <button class="dropdown-item export-cvs" type="button">Cvs</button>
                </div>
              </div>
              <!-- Export Dropdown End -->

              <!-- Length Start -->
              <div class="dropdown-as-select d-inline-block datatable-length" data-datatable="#datatableRowsServerSide" data-childSelector="span">
                <button class="btn p-0 shadow" type="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-bs-offset="0,3">
                  <span
                    class="btn btn-foreground-alternate dropdown-toggle"
                    data-bs-toggle="tooltip"
                    data-bs-placement="top"
                    data-bs-delay="0"
                    title="Item Count"
                  >
                    10 Items
                  </span>
                </button>
                <div class="dropdown-menu shadow dropdown-menu-end">
                  <a class="dropdown-item" href="#">5 Items</a>
                  <a class="dropdown-item active" href="#">10 Items</a>
                  <a class="dropdown-item" href="#">20 Items</a>
                </div>
              </div>
              <!-- Length End -->
            </div>
          </div>
        </div>
        <!-- Controls End --> --}}

        <!-- Table Start -->
        <div class="data-table-responsive-wrapper">
          <table id="datatableRowsServerSide" class="data-table nowrap hover">
            <thead>
              <tr>
                <th class="text-muted text-small text-uppercase">No</th>
                <th class="text-muted text-small text-uppercase">Username</th>
                <th class="text-muted text-small text-uppercase">Name</th>
                <th class="text-muted text-small text-uppercase">Email</th>
                <th class="text-muted text-small text-uppercase">Created</th>
                <th class="text-muted text-small text-uppercase">Updated</th>
                {{-- <th class="empty">&nbsp;</th> --}}
              </tr>
            </thead>
          </table>
        </div>
        <!-- Table End -->
    </div>
    <!-- Content End -->

    <!-- Add Edit Modal Start -->
    <div class="modal modal-right fade" id="addEditModal" tabindex="-1" role="dialog" aria-labelledby="modalTitle" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                <h5 class="modal-title" id="modalTitle">Add New</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                @include('template.acorn.sample.create_edit')

                
            </div>
        </div>
    </div>
    <!-- Add Edit Modal End -->
@endsection

@section('js')
<script src="{{ asset('acorn/js/vendor/datatables.min.js') }}"></script>
{{-- <script src="{{ asset('assets_macb4/app-assets/vendors/js/tables/datatable/datatables.min.js')}}"></script>
<script src="{{ asset('assets_macb4/app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js')}}"></script> --}}
{{-- <script src="{{ asset('acorn/js/cs/datatable.extend.js') }}"></script> --}}
{{-- <script src="{{ asset('acorn/js/plugins/datatable.serverside.js') }}"></script> --}}

<script>
    var urlApi = base_url + '/mitra';
    $(function() {
        $('#datatableRowsServerSide').DataTable({
            responsive: true,
            // "language": {
            //     "processing": "<div class='circle-loader'></div>" //add a loading image,simply putting <img src="loader.gif" /> tag.
            // },
            // filter : true,
            processing: true,
            serverSide: true,
            "filter": true,

            ajax: urlApi + '/list',
            columns: [
                { data: 'id',
                    render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                { data: 'username', name: 'username' },
                { data: 'fullname', fullname: 'fullname' },
                { data: 'email', name: 'email' },
                { data: 'created_at', name: 'created_at' },
                { data: 'updated_at', name: 'updated_at' }
            ]
        });
    });


    // var urlApi = base_url + '/mitra';
    // var buttons = ['copy', 'excel', 'csv', 'print'];
    // var columns = [
    //                 {data: 'username'}, 
    //                 {data: 'fullname'}, 
    //                 {data: 'email'}, 
    //                 {data: 'username'}, 
    //                 {data: 'username'}, 
    //                 {data: 'username', name: 'username'}
    //             ];
    // var columnDefs = [];

    // var urlApi = "https://node-api.coloredstrategies.com/products";
    // var columns = [{data: 'Name'}, {data: 'Sales'}, {data: 'Stock'}, {data: 'Category'}, {data: 'Tag'}, {data: 'Check'}];
    // var columnDefs = [
    //                     // Adding Name content as an anchor with a target #
    //                     {
    //                         targets: 0,
    //                         render: function (data, type, row, meta) {
    //                             return '<a class="list-item-heading body" href="#">' + data + '</a>';
    //                         },
    //                     },
    //                     // Adding Tag content as a span with a badge class
    //                     {
    //                         targets: 4,
    //                         render: function (data, type, row, meta) {
    //                             return '<span class="badge bg-outline-primary">' + data + '</span>';
    //                         },
    //                     },
    //                     // Adding checkbox for Check column
    //                     {
    //                         targets: 5,
    //                         render: function (data, type, row, meta) {
    //                             return '<div class="form-check float-end mt-1"><input type="checkbox" class="form-check-input"></div>';
    //                         },
    //                     },
    //                 ];


    // $( document ).ready(function() {
    //     this._datatable = jQuery('#datatableRowsServerSide').DataTable({
    //         scrollX: true,
    //         buttons: buttons,
    //         info: false,
    //         processing: true,
    //         serverSide: true,
    //         ajax: urlApi + '/products',
    //         sDom: '<"row"<"col-sm-12"<"table-container"t>r>><"row"<"col-12"p>>', // Hiding all other dom elements except table and pagination
    //         pageLength: 10,
    //         columns: columns,
    //         language: {
    //             paginate: {
    //             previous: '<i class="cs-chevron-left"></i>',
    //             next: '<i class="cs-chevron-right"></i>',
    //             },
    //         },
    //         initComplete: function (settings, json) {
    //             _this._setInlineHeight();
    //         },
    //         drawCallback: function (settings) {
    //             _this._setInlineHeight();
    //         },
    //         columnDefs: [
    //             // Adding Name content as an anchor with a target #
    //             {
    //                 targets: 0,
    //                 render: function (data, type, row, meta) {
    //                     return '<a class="list-item-heading body" href="#">' + data + '</a>';
    //                 },
    //             },
    //             // Adding Tag content as a span with a badge class
    //             {
    //                 targets: 4,
    //                 render: function (data, type, row, meta) {
    //                     return '<span class="badge bg-outline-primary">' + data + '</span>';
    //                 },
    //             },
    //             // Adding checkbox for Check column
    //             {
    //                 targets: 5,
    //                 render: function (data, type, row, meta) {
    //                     return '<div class="form-check float-end mt-1"><input type="checkbox" class="form-check-input"></div>';
    //                 },
    //             },
    //         ],
    //     });

    // });

    
</script>

@endsection