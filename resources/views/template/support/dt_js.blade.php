    <!-- BEGIN: Page Vendor JS-->
    <script src="{{ asset('assets_macb4/app-assets/vendors/js/tables/datatable/datatables.min.js')}}"></script>
    <script src="{{ asset('assets_macb4/app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js')}}"></script>
    {{-- <script src="{{ asset('assets_macb4/app-assets/vendors/js/tables/datatable/dataTables.buttons.min.js')}}"></script> --}}
    {{-- <script src="{{ asset('assets_macb4/app-assets/vendors/js/tables/buttons.flash.min.js')}}"></script> --}}
    {{-- <script src="{{ asset('assets_macb4/app-assets/vendors/js/tables/jszip.min.js')}}"></script> --}}
    {{-- <script src="{{ asset('assets_macb4/app-assets/vendors/js/tables/pdfmake.min.js')}}"></script> --}}
    {{-- <script src="{{ asset('assets_macb4/app-assets/vendors/js/tables/vfs_fonts.js')}}"></script> --}}
    {{-- <script src="{{ asset('assets_macb4/app-assets/vendors/js/tables/buttons.html5.min.js')}}"></script> --}}
    {{-- <script src="{{ asset('assets_macb4/app-assets/vendors/js/tables/buttons.print.min.js')}}"></script> --}}
    <!-- END: Page Vendor JS-->


    <!-- BEGIN: Page JS-->
    {{-- <script src="{{ asset('assets_macb4/app-assets/js/scripts/tables/datatables/datatable-advanced.js')}}"></script> --}}
    <!-- END: Page JS-->
    {{-- <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script> --}}

    
    <script src="{{ asset('assets_macb4/assets/js/sweetalert2@11.js')}}"></script>