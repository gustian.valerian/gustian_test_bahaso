@if (session('alert-success'))
      <script>
        var ptitle  = 'Pendidikan';
        var ptype   = 'success';
        var pmessage = {!! json_encode(session('alert-success')) !!};
        custommessage(ptitle, pmessage, ptype);
      </script>
@endif

@if (session('alert-success'))
      <script>
        var ptitle  = 'Pendidikan';
        var ptype   = 'failed';
        var pmessage = {!! json_encode(session('alert-success')) !!};
        custommessage(ptitle, pmessage, ptype);
      </script>
@endif