@extends('template.acorn')

@section('title',' User')


@section('css_vendor')
<link rel="stylesheet" href="{{ asset('acorn/css/vendor/datatables.min.css') }}" />

@endsection

@section('css')

@endsection

@section('breadcrumbs')
  <!-- Title and Top Buttons Start -->
  <div class="page-title-container">
    <div class="row">
      <!-- Title Start -->
      <div class="col-12 col-md-7">
        <h1 class="mb-0 pb-0 display-4" id="title">Kursus</h1>
        <nav class="breadcrumb-container d-inline-block" aria-label="breadcrumb">
          <ul class="breadcrumb pt-0">
            <li class="breadcrumb-item"><a href="{{ url('dashboard') }}">Home</a></li>
            <li class="breadcrumb-item"><a href="{{ url('kursus') }}">Kursus</a></li>
          </ul>
        </nav>
      </div>
      <!-- Title End -->

      <!-- Top Buttons Start -->
      
      <!-- Top Buttons End -->
    </div>
  </div>
  <!-- Title and Top Buttons End -->
@endsection

@section('content')
  <!-- Content Start -->
  <div class="data-table-rows slim">

    <!-- Table Start -->
    <div class="data-table-responsive-wrapper">
        <table id="dtServerSide" class="data-table nowrap hover">
          <thead>
            <tr>
              <th class="text-muted text-small text-uppercase">No</th>
              <th class="text-muted text-small text-uppercase">Kursus</th>
              <th class="text-muted text-small text-uppercase">Peserta</th>
              <th class="text-muted text-small text-uppercase">Status</th>
              <th class="text-muted text-small text-uppercase">Action</th>
              {{-- <th class="empty" width="15%">&nbsp;</th> --}}
            </tr>
          </thead>
        </table>
      </div>
    <!-- Table End -->
  </div>
  <!-- Content End -->

@endsection

@section('js_vendor')
<script src="{{ asset('acorn/js/cs/datatable.extend.js') }}"></script>
<script src="{{ asset('acorn/js/plugins/datatable.serverside.js') }}"></script>
<script src="{{ asset('additional/fungsi/crud.js') }}"></script>
@endsection

@section('js')
<script>


    datatable = {
        table_id : 'dtServerSide',
        url : base_url + '/sertifikat',
        column: [
            { data: 'id',
                render: function (data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                }
            },
            { data: 'artikel.artikel' },
            { data: 'peserta.fullname' },
            { data: 'statuscourse.value' },
            { data: 'id', //primary key dari tabel
            render: function(data, type, row)
                {
                  console.log(data);
                    let buttonAction =  ` <div class="btn-group">
                        <button type="button" class="btn btn-sm btn-light dropdown-toggle" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          More
                        </button>
                        <div class="dropdown-menu">`;
                            if(row.sertifikat){
                                buttonAction +=   '<a class="dropdown-item" href="'+ row.sertifikat +'">Sertifikat</a>' ;
                            }
                           
                    buttonAction += `</div>
                      </div>`;
                   
                
                return buttonAction;
                } 
            }
        ],
        columnDefs: [],
    };
</script>



@endsection