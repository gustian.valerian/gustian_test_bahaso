@extends('template.macb4')

@section('title',' Profile')

@section('css_vendor')
    
@endsection

@section('css_custom')
    {{-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">  --}}

@endsection

@section('breadcumbs')
    <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a>
    </li>
    <li class="breadcrumb-item active">Profile
    </li>
@endsection

@section('body')
<!-- Column rendering table -->
<section id="column">
    <div class="row">
       @include('template.session_list')
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Admin</h3>
                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                            {{-- <li><a data-action="close"><i class="ft-x"></i></a></li> --}}
                        </ul>
                    </div>
                </div>
                <div class="card-content collapse show">
                    <div class="card-body card-dashboard dataTables_wrapper dt-bootstrap">
                        <form class="kt-form kt-form--label-right" id="kt_form_1" method="POST" 
                        action="/profile/admin" enctype="multipart/form-data">
                            @csrf

                            <div class="row px-2">
                                <div class="col-12 col-sm-6">

                                    <div class="form-group validated is-invalid">
                                        <label>Photo</label>
                                        <div class="custom-file">
                                            <input type="file" name="path_photo" class="form-control" >
                                            @error('path_photo')
                                                <div class="error invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label>NIK</label>
                                            <input type="text" class="form-control @error('nik') is-invalid @enderror" value="{{ old('nik')?? $model->nik_karyawan ?? ''}}" name="nik">
                                            @error('nik')
                                            <div class="error invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>

                                    <div class="form-group">
                                        <label>Nama Lengkap</label>
                                            <input type="text" class="form-control @error('fullname') is-invalid @enderror" value="{{ old('fullname')?? $model->user->fullname??'' }}" name="fullname">
                                            @error('fullname')
                                            <div class="error invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>

                                    <div class="form-group">
                                        <label>Username</label>
                                            <input type="text" class="form-control @error('username') is-invalid @enderror" value="{{ old('username')?? $user->username}}" name="username">
                                            @error('username')
                                            <div class="error invalid-feedback">{{ $message }}</div>
                                         @enderror
                                    </div>

                                    <div class="form-group">
                                        <label>Password</label>
                                            <input type="text" class="form-control @error('password') is-invalid @enderror" value="{{ old('password')}}" name="password">
                                            @error('password')
                                            <div class="error invalid-feedback">{{ $message }}</div>
                                         @enderror
                                    </div>

                                    <div class="form-group">
                                        <label>Email</label>
                                            <input type="text" class="form-control @error('email') is-invalid @enderror" value="{{ old('email')?? $model->email??'' }}" name="email">
                                            @error('email')
                                            <div class="error invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>

                                    <div class="form-group">
                                        <label>Nomer Telphone</label>
                                            <input type="text" class="form-control @error('no_telp_karyawan') is-invalid @enderror" value="{{ old('no_telp_karyawan')??$model->nomor_telp_karyawan ??''  }}" name="no_telp_karyawan">
                                                @error('no_telp_karyawan')
                                            <div class="error invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
    
                                </div>{{-- end col --}}

                                <div class="col-12 col-sm-6">

                                    <div class="form-group">
                                        <label>Alamat</label>
                                            <textarea name="alamat_karyawan" class="form-control" id="alamat_karyawan" placeholder="Masukan Alamat" cols="10" rows="5">{{ $model->alamat_karyawan ??'' }}</textarea>
                                                @error('alamat_karyawan')
                                            <div class="error invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>

                                    <div class="form-group">
                                        <label>Nomer WhatsApp</label>
                                            <input type="text" class="form-control @error('no_telp_wa_karyawan') is-invalid @enderror" value="{{ old('no_telp_wa_karyawan')?? $model->nomor_telp_wa_karyawan ??''}}" name="no_telp_wa_karyawan">
                                                @error('no_telp_wa_karyawan')
                                            <div class="error invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>

                                    <div class="form-group">
                                        <label>Tanggal Lahir</label>
                                            <input type="date" class="form-control @error('tanggal_lahir_karyawan') is-invalid @enderror" value="{{ old('tanggal_lahir_karyawan')?? $model->tanggal_lahir_karyawan ??''}}" name="tanggal_lahir_karyawan">
                                            @error('tanggal_lahir_karyawan')
                                            <div class="error invalid-feedback">{{ $message }}</div>
                                            @enderror
                                    </div>

                                    <div class="form-group">
                                        <label>Jenis Kelamin</label>
                                        <select name="jenis_kelamin_karyawan" id="jenis_kelamin_karyawan" class="form-control @error('jenis_kelamin_karyawan') is-invalid @enderror" value="{{ old('jenis_kelamin_karyawan') }}">
                                            @if (empty($model->jenis_kelamin_karyawan))
                                            <option selected>-- Pilih jenis kelamin --</option>  
                                            @endif
                                            @foreach($enum as $row)
                                            @if ($row->category == 'Jenis kelamin')
                                            <?php if (empty($model->jenis_kelamin_karyawan)) {?>
                                               
                                                <option value="{{ $row->key }}">{{ $row->value }}</option>  

                                                <?php }elseif(!empty($model->jenis_kelamin_karyawan)){?>
                                                    
                                                    <option value="{{ $row->key }}" {{( $row->key ==  $model->jenis_kelamin_karyawan) ? "selected" :""}}>{{  $row->value  }}</option>  
                                            <?php }?>
                                            @endif
                                            @endforeach
                                        </select>
                                        @error('jenis_kelamin_karyawan')
                                        <div class="error invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div> 

                                </div>          
                            </div><!-- end row -->
                            
                            <div class="text-right mt-3">
                                <button type="button" class="btn btn-secondary" onclick="history.back();">Cancel</button>
                                <button type="submit" class="btn btn-primary" >Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Column rendering table -->

@endsection

@section('js_vendor')

@endsection

@section('js_custom')


<script>
    
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#src_image').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#path_image").change(function(){
        readURL(this);
    });

    // $(function() {
    //     $("form[name='post_data']").validate({
    //         rules: {
    //             jenis_arsip: "required",
                
    //         },
    //         messages: {
    //             jenis_arsip: "Kolom tidak boleh kosong",
    //         },
    //         submitHandler: function(form) {
    //         form.submit();
    //         }
    //     });
    // });
</script>
@endsection