@extends('template.macb4')

@section('title', 'Profile')

@section('css_vendor')
    
@endsection

@section('css_custom')
    {{-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">  --}}

@endsection

@section('breadcumbs')
<li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a>
</li>
<li class="breadcrumb-item active">Profile
</li>
@endsection

@section('body')
<!-- Column rendering table -->
<section id="column">
    <div class="row">
       @include('template.session_list')
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Mitra</h3>
                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">
                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                            {{-- <li><a data-action="close"><i class="ft-x"></i></a></li> --}}
                        </ul>
                    </div>
                </div>
                <div class="card-content collapse show">
                    <div class="card-body card-dashboard dataTables_wrapper dt-bootstrap">
                        <form class="kt-form kt-form--label-right" id="kt_form_1" method="POST" 
                        action="{{ url('/profile/mitra') }}" enctype="multipart/form-data">
                            @csrf

                            <div class="row px-2">
                                <div class="col-12 col-sm-6">
                                    
                                    @if (!empty($userdata->usermitra->foto_mitra))
                                    <div class="rounded mb-3">
                                        <img id="myImg" src="{{ asset('/storage/photomitra/'.$userdata->usermitra->foto_mitra) }}" height="200" width="200"/>
                                    </div>
                                    @else
                                    <div class="rounded mb-3">
                                        <img id="myImg" src="{{ asset('/storage/photomitra/no_image.png') }}" height="200" width="200"/>
                                    </div>
                                    @endif

                                <div class="form-group">
                                    <label>Nik Mitra</label>
                                        <input readonly type="text" class="form-control @error('nik') is-invalid @enderror" value="{{ old('nik')?? $userdata->usermitra->nik_mitra }}" name="nik">
                                        @error('nik')
                                        <div class="error invalid-feedback">{{ $message }}</div>
                                     @enderror
                                </div>

                                <div class="form-group">
                                    <label>Nama Lengkap Mitra</label>
                                        <input readonly type="text" class="form-control @error('fullname') is-invalid @enderror" value="{{ old('fullname')?? $userdata->fullname }}" name="fullname">
                                        @error('fullname')
                                        <div class="error invalid-feedback">{{ $message }}</div>
                                     @enderror
                                </div>

                                <div class="form-group">
                                    <label>Username Mitra</label>
                                        <input readonly type="text" class="form-control @error('username') is-invalid @enderror" value="{{ old('username')?? $userdata->username }}" name="username">
                                        @error('username')
                                        <div class="error invalid-feedback">{{ $message }}</div>
                                     @enderror
                                </div>

                                <div class="form-group">
                                    <label>Password Mitra</label>
                                        <input type="text" class="form-control @error('password') is-invalid @enderror" value="{{ old('password') }}" name="password">
                                        @error('password')
                                        <div class="error invalid-feedback">{{ $message }}</div>
                                     @enderror
                                </div>

                                <div class="form-group">
                                    <label>Email Mitra</label>
                                        <input readonly type="text" class="form-control @error('email') is-invalid @enderror" value="{{ old('email')?? $userdata->usermitra->email }}" name="email">
                                        @error('email')
                                        <div class="error invalid-feedback">{{ $message }}</div>
                                     @enderror
                                </div>

                                <div class="form-group">
                                    <label>Nomer Telphone Mitra</label>
                                        <input readonly type="text" class="form-control @error('no_telp_mitra') is-invalid @enderror" value="{{ old('no_telp_mitra')?? $userdata->usermitra->nomor_telp_mitra }}" name="no_telp_mitra">
                                            @error('no_telp_mitra')
                                        <div class="error invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label>Booster</label>
                                        <input disabled type="text" class="form-control @error('booster_text') is-invalid @enderror" value="{{ old('booster_text')?? $userdata->usermitra->booster_text }}" name="booster_text">
                                            @error('booster_text')
                                        <div class="error invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label>Booster Date</label>
                                        <input type="date" disabled class="form-control @error('booster_date') is-invalid @enderror" value="{{ old('booster_date')?? $userdata->usermitra->booster_date }}" name="booster_date">
                                        @error('booster_date')
                                        <div class="error nvalid-feedback">{{ $message }}</div>
                                        @enderror
                                </div>

                                <div class="form-group">
                                    <label>Jenis Kelamin Customer</label>
                                    <input disabled class="form-control"  type="text" value="{{ $kelamin_customer }}">     
                                    {{-- <input  class="form-control"  type="hidden" name="jenis_kelamin_customer" value="{{ $kelamin_customer->id }}">      --}}
                                    @error('jenis_kelamin_customer')
                                    <div class="error invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div> 

    
                                </div>{{-- end col --}}

                                <div class="col-12 col-sm-6">
                                    
                                    <div class="form-group">
                                        <label>Rekening</label>
                                            <textarea readonly name="rekening" class="form-control @error('rekening') is-invalid @enderror" id="rekening" placeholder="Masukan Rekening" cols="10" rows="5">{{ $userdata->usermitra->rekening }}</textarea>
                                                @error('rekening')
                                            <div class="error invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>

                                    <div class="form-group">
                                        <label>Alamat Mitra</label>
                                            <textarea readonly name="alamat_mitra" class="form-control" id="alamat_mitra" placeholder="Masukan Alamat" cols="10" rows="5">{{ $userdata->usermitra->alamat_mitra }}</textarea>
                                                @error('alamat_mitra')
                                            <div class="error invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>

                                    <div class="form-group">
                                        <label>Nomer WhatsApp Mitra</label>
                                            <input readonly type="text" class="form-control @error('no_telp_wa_mitra') is-invalid @enderror" value="{{ old('no_telp_wa_mitra')?? $userdata->usermitra->nomor_telp_wa_mitra }}" name="no_telp_wa_mitra">
                                                @error('no_telp_wa_mitra')
                                            <div class="error invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>

                                    <div class="form-group">
                                        <label>Tanggal Lahir Mitra</label>
                                            <input readonly type="date" class="form-control @error('tanggal_lahir_mitra') is-invalid @enderror" value="{{ old('tanggal_lahir_mitra')?? $userdata->usermitra->tanggal_lahir_mitra }}" name="tanggal_lahir_mitra">
                                            @error('tanggal_lahir_mitra')
                                            <div class="error invalid-feedback">{{ $message }}</div>
                                            @enderror
                                    </div>

                                        
                                    <div class="form-group">
                                        <label>Status Vaksin Mitra</label>    
                                            <input disabled class="form-control" type="text" value="{{ $vaksin->value }}">         
                                            <input  class="form-control" type="hidden" name="vaksin" value="{{ $vaksin->id }}">         
                                            @error('vaksin')
                                                <div class="error invalid-feedback">{{ $message }}</div>
                                            @enderror
                                    </div>

                                    <div class="form-group">
                                        <label>Jenis Kelamin Mitra</label>
                                        <input disabled class="form-control" type="text"  value="{{ $kelamin_mitra->value }}">     
                                        <input class="form-control" type="hidden" name="jenis_kelamin_mitra"  value="{{ $kelamin_mitra->id }}">     
                                        @error('jenis_kelamin_mitra')
                                        <div class="error invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div> 

                                    <div class="form-group">
                                        <label>Status mitra</label> 
                                            @foreach ($enum as $row )
                                            @if ( $row->id == $userdata->usermitra->status_mitra)
                                            <input readonly class="form-control" type="text"  value="{{ $row->value }}"> 
                                            <input type="hidden" name="status_mitra" value="{{ $row->id }}">
                                            @endif           
                                            @endforeach     
                                        </select>
                                       
                                        @error('status_mitra')
                                        <div class="error invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div> 
  
                                     <div class="form-check">
                                        <label for="defaultCheck1" class="mr-3">Layanan Mitra Available</label> <br>
                                        
                                        @foreach($layanan as $row)
                                        <input readonly class="form-check-input" disabled type="checkbox" name="layanan[][layanan]" value="{{ $row->id }}"  
                                        {{ in_array($row->id, array_column($userdata->usermitra->maplayanan->toArray(), 'layanan_id' )) ? 'checked' : '' }}
                                        id="defaultCheck1">
                                        <label class="form-check-label" for="defaultCheck1">  
                                            {{ $row->nama_layanan }}
                                        </label><br>
                                        @endforeach
                                      </div> 

                                </div>          
                            </div><!-- end row -->

                            
                            <div class="text-right mt-3">
                                <button type="button" class="btn btn-secondary" onclick="history.back();">Cancel</button>
                                <button type="submit" class="btn btn-primary" >Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Column rendering table -->

@endsection

@section('js_vendor')

@endsection

@section('js_custom')


<script>
    
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#src_image').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#path_image").change(function(){
        readURL(this);
    });

    // $(function() {
    //     $("form[name='post_data']").validate({
    //         rules: {
    //             jenis_arsip: "required",
                
    //         },
    //         messages: {
    //             jenis_arsip: "Kolom tidak boleh kosong",
    //         },
    //         submitHandler: function(form) {
    //         form.submit();
    //         }
    //     });
    // });
</script>
@endsection