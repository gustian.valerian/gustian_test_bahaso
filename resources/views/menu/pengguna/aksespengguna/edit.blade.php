@extends('template.acorn')

@section('title',' Akses Pengguna')


@section('css_vendor')
<link rel="stylesheet" href="{{ asset('acorn/css/vendor/select2.min.css') }}" />
<link rel="stylesheet" href="{{ asset('acorn/css/vendor/select2-bootstrap4.min.css') }}" />
@endsection

@section('css')

@endsection

@section('breadcrumbs')
  <!-- Title and Top Buttons Start -->
  <div class="page-title-container">
    <div class="row">
      <!-- Title Start -->
      <div class="col-12 col-md-7">
        <h1 class="mb-0 pb-0 display-4" id="title">Master User</h1>
        <nav class="breadcrumb-container d-inline-block" aria-label="breadcrumb">
          <ul class="breadcrumb pt-0">
            <li class="breadcrumb-item"><a href="{{ url('dashboard') }}">Home</a></li>
            <li class="breadcrumb-item"><a href="{{ url('userakses') }}">Akses Pengguna</a></li>
            <li class="breadcrumb-item"><a href="#">Edit</a></li>
            {{-- <li class="breadcrumb-item"><a href="Interface.Plugins.html">Plugins</a></li>
            <li class="breadcrumb-item"><a href="Interface.Plugins.Datatables.html">Datatables</a></li> --}}
          </ul>
        </nav>
      </div>
      <!-- Title End -->
    </div>
  </div>
  <!-- Title and Top Buttons End -->
@endsection

@section('content')

    <section class="scroll-section" id="bootstrapServerSide">
        {{-- <h2 class="small-title">Bootstrap Server Side</h2> --}}
        <div class="card">
            <div class="card-body">
                <form class="row g-3" method="POST" 
                action="{{ route('userakses_update_update', $model->id)}}" name="post_data">
                @csrf
                @method('PUT')
                
                    <div class="col-md-4">
                        <label for="" class="form-label">Nama Akses Pengguna</label>
                        <input type="text"  class="form-control @error('name') is-invalid @enderror" value="{{ old('name', $model->name) }}" name="name">
                        @error('name')
                            <div class="valid-feedback">{{ $message }}</div>
                        @enderror
                    </div>

                    <div class="form-group">
                        {{-- <h4 class="card-title">Square Skin iCheck</h4> --}}
                        {{-- <div class="col-md-6 col-sm-2"> --}}
                            <table class="table table-hover col-12">
                                <thead>
                                    <tr class="bg-primary text-white text-center">
                                        <th class="border-3 border-white text-white">Description</th>
                                        <th class="border-3 border-white text-white">View</th>
                                        <th class="border-3 border-white text-white">Create</th>
                                        <th class="border-3 border-white text-white">Update</th>
                                        <th class="border-3 border-white text-white">Delete</th>
                                        <th class="border-3 border-white text-white">Import</th>
                                        <th class="border-3 border-white text-white">Export</th>
                                        <th class="border-3 border-white text-white">All</th>
                                    </tr>
                                </thead>
                                <tbody class="text-center skin skin-square">
                                    @foreach ($listmenu as $item)
                                        @if ($item->sort == '#')
                                            <tr class="bg-warning">
                                                <td class="text-white" colspan="8"><b>{{ $item->menuname }}</b></td>
                                                
                                            </tr>
                                        @else
                                            <tr>
                                                <?php
                                                    $data = $model->mapgroupmenu->where('id_menus', $item->id)->where('id_groups', $model->id)->first()
                                                ?>
                                                {{-- {{ $model->mapgroupmenu->allow_view }} --}}
                                                <td class="text-left">{{ $item->menuname }}</td>
                                                <td><input class="checkSingle master_view" type="checkbox" name="privilege[{{ $item->id }}][view]"
                                                    {{ $data->allow_view ?? false == 1 ? 'checked' : '' }}></td>
                                                <td><input class="checkSingle master" type="checkbox" name="privilege[{{ $item->id }}][create]"
                                                    {{ $data->allow_create ?? false == 1 ? 'checked' : '' }}></td>
                                                <td><input class="checkSingle master" type="checkbox" name="privilege[{{ $item->id }}][update]"
                                                    {{ $data->allow_update ?? false == 1 ? 'checked' : '' }}></td>
                                                <td><input class="checkSingle master" type="checkbox" name="privilege[{{ $item->id }}][delete]"
                                                    {{ $data->allow_delete ?? false == 1 ? 'checked' : '' }}></td>
                                                <td><input class="checkSingle master" type="checkbox" name="privilege[{{ $item->id }}][import]"
                                                    {{ $data->allow_import ?? false == 1 ? 'checked' : '' }}></td>
                                                <td><input class="checkSingle master" type="checkbox" name="privilege[{{ $item->id }}][export]"
                                                    {{ $data->allow_export ?? false == 1 ? 'checked' : '' }}></td>
                                                <td><input type="checkbox" class="checkedAll"></td>
                                            </tr>
                                        @endif
                                       
                                    @endforeach

                                </tbody>
                            </table>
                            
                            
                        {{-- </div> --}}
                    </div>

                    

                    <div class="text-right mt-3 col-12">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection

@section('js_vendor')
<script src="{{ asset('acorn/js/forms/validation.js') }}"></script>
<script src="{{ asset('acorn/js/vendor/select2.full.min.js') }}"></script>
<script src="{{ asset('acorn/js/forms/controls.select2.js') }}"></script>
@endsection

@section('js')
<script>
// jQuery('#select2Basic').select2({});
</script>

<script>
    $( document ).ready(function() {
        checkbox();
    });

    function checkbox(){
        $(".checkedAll").change(function(){
            console.log('all check');
            if(this.checked){
                $(this).parents('tr').find(".checkSingle").each(function(){
                    this.checked=true;
                })              
            }else{
                $(this).parents('tr').find(".checkSingle").each(function(){
                    this.checked=false;
                })              
            }
        });

        $(".checkSingle").click(function () {
            if ($(this).is(":checked")){
                var isAllChecked = 0;
                $(this).parents('tr').find(".checkSingle").each(function(){
                    if(!this.checked)
                    isAllChecked = 1;
                })              
                if(isAllChecked == 0){ $(this).parents('tr').find(".checkedAll").prop("checked", true); }     
            }else {
                $(this).parents('tr').find(".checkedAll").prop("checked", false);
            }
        });

        $(".master_view").click(function () {
            if (!$(this).is(":checked")){
               console.log('uncheck');    
               $(this).parents('tr').find(".checkSingle").each(function(){
                    this.checked=false;
                }) 
            }
        });

        $(".master").click(function () {
            if ($(this).is(":checked")){
               $(this).parents('tr').find(".master_view").each(function(){
                    this.checked=true;
                }) 
            }
        });
    }

    $(function() {
        $("form[name='post_data']").validate({
            rules: {
                name: "required",
            },
            messages: {
                name: "Kolom tidak boleh kosong",
            },
            submitHandler: function(form) {
            form.submit();
            }
        });
    });

</script>

@endsection