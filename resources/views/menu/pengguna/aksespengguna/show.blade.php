@extends('template.acorn')

@section('title',' Akses Pengguna')


@section('css_vendor')

@endsection

@section('css')

@endsection

@section('breadcrumbs')
  <!-- Title and Top Buttons Start -->
  <div class="page-title-container">
    <div class="row">
      <!-- Title Start -->
      <div class="col-12 col-md-7">
        <h1 class="mb-0 pb-0 display-4" id="title">Master User</h1>
        <nav class="breadcrumb-container d-inline-block" aria-label="breadcrumb">
          <ul class="breadcrumb pt-0">
            <li class="breadcrumb-item"><a href="{{ url('dashboard') }}">Home</a></li>
            <li class="breadcrumb-item"><a href="{{ url('userakses') }}">Akses Pengguna</a></li>
            <li class="breadcrumb-item"><a href="#">Show</a></li>
            {{-- <li class="breadcrumb-item"><a href="Interface.Plugins.html">Plugins</a></li>
            <li class="breadcrumb-item"><a href="Interface.Plugins.Datatables.html">Datatables</a></li> --}}
          </ul>
        </nav>
      </div>
      <!-- Title End -->
    </div>
  </div>
  <!-- Title and Top Buttons End -->
@endsection

@section('content')
    <section class="scroll-section" id="bootstrapServerSide">
        {{-- <h2 class="small-title">Bootstrap Server Side</h2> --}}
        <div class="card">
            <div class="card-body">
                <form class="row g-3">
                    <div class="col-md-4">
                        <label>Nama Akses Pengguna</label>
                        <label class="form-control">{{ $model->name }}</label>
                    </div>

                    <div class="form-group">
                        {{-- <h4 class="card-title">Square Skin iCheck</h4> --}}
                        {{-- <div class="col-md-6 col-sm-2"> --}}
                            <table class="table table-hover col-12">
                                <thead>
                                    <tr class="bg-primary text-white text-center">
                                        <th class="border-3 border-white text-white">Description</th>
                                        <th class="border-3 border-white text-white">View</th>
                                        <th class="border-3 border-white text-white">Create</th>
                                        <th class="border-3 border-white text-white">Update</th>
                                        <th class="border-3 border-white text-white">Delete</th>
                                        <th class="border-3 border-white text-white">Import</th>
                                        <th class="border-3 border-white text-white">Export</th>
                                        {{-- <th class="border-3 border-white text-white">All</th> --}}
                                    </tr>
                                </thead>
                                <tbody class="text-center skin skin-square">
                                    @foreach ($listmenu as $item)
                                        @if ($item->sort == '#')
                                            <tr class="bg-warning">
                                                <td class="text-white" colspan="6"><b>{{ $item->menuname }}</b></td>
                                                <td><input type="checkbox" id="input-10" disabled></td>
                                            </tr>
                                        @else
                                            <tr>
                                                <?php
                                                    $data = $model->mapgroupmenu->where('id_menus', $item->id)->where('id_groups', $model->id)->first()
                                                ?>
                                                <td class="text-left">{{ $item->menuname }}</td>
                                                <td>
                                                    <input type="checkbox" name="privilege[{{ $item->id }}][view]" id="input-10" 
                                                    {{ $data->allow_view ?? false == 1 ? 'checked' : '' }}  disabled>
                                                </td>
                                                <td>
                                                    <input type="checkbox" name="privilege[{{ $item->id }}][create]" id="input-10"
                                                    {{ $data->allow_create ?? false == 1 ? 'checked' : '' }}  disabled>
                                                </td>
                                                <td>
                                                    <input type="checkbox" name="privilege[{{ $item->id }}][update]" id="input-10"
                                                    {{ $data->allow_update ?? false == 1 ? 'checked' : '' }}  disabled>
                                                </td>
                                                <td>
                                                    <input type="checkbox" name="privilege[{{ $item->id }}][delete]" id="input-10"
                                                    {{ $data->allow_delete ?? false == 1 ? 'checked' : '' }}  disabled>
                                                </td>
                                                <td>
                                                    <input type="checkbox" name="privilege[{{ $item->id }}][import]" id="input-10"
                                                    {{ $data->allow_import ?? false == 1 ? 'checked' : '' }}  disabled>
                                                </td>
                                                <td>
                                                    <input type="checkbox" name="privilege[{{ $item->id }}][export]" id="input-10"
                                                    {{ $data->allow_export ?? false == 1 ? 'checked' : '' }}  disabled>
                                                </td>
                                            </tr>
                                        @endif
                                       
                                    @endforeach


                                </tbody>
                            </table>
                            
                            
                        {{-- </div> --}}
                    </div>

                    <div class="text-right mt-3 col-12">
                        <button type="button" class="btn btn-secondary" onclick="history.back();">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection

@section('js_vendor')

@endsection

@section('js')
<script>

</script>



@endsection