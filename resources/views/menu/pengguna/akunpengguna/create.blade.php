@extends('template.acorn')

@section('title',' User')


@section('css_vendor')
<link rel="stylesheet" href="{{ asset('acorn/css/vendor/select2.min.css') }}" />
<link rel="stylesheet" href="{{ asset('acorn/css/vendor/select2-bootstrap4.min.css') }}" />
@endsection

@section('css')

@endsection

@section('breadcrumbs')
  <!-- Title and Top Buttons Start -->
  <div class="page-title-container">
    <div class="row">
      <!-- Title Start -->
      <div class="col-12 col-md-7">
        <h1 class="mb-0 pb-0 display-4" id="title">Master User</h1>
        <nav class="breadcrumb-container d-inline-block" aria-label="breadcrumb">
          <ul class="breadcrumb pt-0">
            <li class="breadcrumb-item"><a href="{{ url('dashboard') }}">Home</a></li>
            <li class="breadcrumb-item"><a href="{{ url('useraccount') }}">Akun Pengguna</a></li>
            <li class="breadcrumb-item"><a href="#">Edit</a></li>
            {{-- <li class="breadcrumb-item"><a href="Interface.Plugins.html">Plugins</a></li>
            <li class="breadcrumb-item"><a href="Interface.Plugins.Datatables.html">Datatables</a></li> --}}
          </ul>
        </nav>
      </div>
      <!-- Title End -->
    </div>
  </div>
  <!-- Title and Top Buttons End -->
@endsection

@section('content')

    <section class="scroll-section" id="bootstrapServerSide">
        {{-- <h2 class="small-title">Bootstrap Server Side</h2> --}}
        <div class="card">
            <div class="card-body">
                <form class="row g-3" method="POST" 
                action="{{ route('useraccount_create_store')}}" name="post_data">
                @csrf
                
                    <div class="col-md-4">
                        <label for="" class="form-label">Username</label>
                        <input type="text"  class="form-control @error('username') is-invalid @enderror" value="{{ old('username') }}" name="username" required>
                        @error('username')
                            <div class="valid-feedback">{{ $message }}</div>
                        @enderror
                    </div>

                    <div class="col-md-4">
                        <label for="" class="form-label">Fullname</label>
                        <input type="text"  class="form-control @error('fullname') is-invalid @enderror" value="{{ old('fullname') }}" name="fullname" required>
                        @error('fullname')
                            <div class="valid-feedback">{{ $message }}</div>
                        @enderror
                    </div>

                    <div class="col-md-4">
                        <label for="" class="form-label">Email</label>
                        <input type="text"  class="form-control @error('email') is-invalid @enderror" value="{{ old('email') }}" name="email" required>
                        @error('email')
                            <div class="valid-feedback">{{ $message }}</div>
                        @enderror
                    </div>

                    <div class="col-md-4">
                        <label for="" class="form-label">Password</label>
                        <input type="password"  class="form-control @error('password') is-invalid @enderror" value="{{ old('password') }}" name="password" required>
                        @error('password')
                            <div class="valid-feedback">{{ $message }}</div>
                        @enderror
                    </div>

                    <div class="col-md-4">
                        <label for="" class="form-label">Re-type Password</label>
                        <input type="password"  class="form-control @error('password_confirm') is-invalid @enderror" value="{{ old('password_confirm') }}" name="password_confirm" required>
                        @error('password_confirm')
                            <div class="valid-feedback">{{ $message }}</div>
                        @enderror
                    </div>

                    <div class="col-md-4">
                        <label for="" class="form-label">Telp Number</label>
                        <input type="text"  class="form-control @error('telpnumber') is-invalid @enderror" value="{{ old('telpnumber') }}" name="telpnumber" required>
                        @error('telpnumber')
                            <div class="valid-feedback">{{ $message }}</div>
                        @enderror
                    </div>

                    <div class="col-md-6">
                        <div class="w-100">
                            <label for="" class="form-label" for="exampleSelect1">Group User</label>
                            <select class="form-control" id="select2Basic" name="group" required>
                                <option selected disabled value="">Choose...</option>
                                @foreach ($group as $group)
                                    <option value="{{ $group->id }}">
                                        {{ $group->name }}
                                    </option>
                                @endforeach
                            </select>
                            @error('group')
                                    <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        
                    </div>

                    <div class="text-right mt-3 col-12">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection

@section('js_vendor')
<script src="{{ asset('acorn/js/forms/validation.js') }}"></script>
<script src="{{ asset('acorn/js/vendor/select2.full.min.js') }}"></script>
<script src="{{ asset('acorn/js/forms/controls.select2.js') }}"></script>
@endsection

@section('js')
<script>
// jQuery('#select2Basic').select2({});
</script>



@endsection