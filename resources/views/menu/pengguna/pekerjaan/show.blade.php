@extends('template.acorn')

@section('title',' User')


@section('css_vendor')

@endsection

@section('css')

@endsection

@section('breadcrumbs')
  <!-- Title and Top Buttons Start -->
  <div class="page-title-container">
    <div class="row">
      <!-- Title Start -->
      <div class="col-12 col-md-7">
        <h1 class="mb-0 pb-0 display-4" id="title">Pekerjaan</h1>
        <nav class="breadcrumb-container d-inline-block" aria-label="breadcrumb">
          <ul class="breadcrumb pt-0">
            <li class="breadcrumb-item"><a href="{{ url('dashboard') }}">Home</a></li>
            <li class="breadcrumb-item"><a href="{{ url('pekerjaan') }}">Pekerjaan</a></li>
            <li class="breadcrumb-item"><a href="#">Show</a></li>
            {{-- <li class="breadcrumb-item"><a href="Interface.Plugins.html">Plugins</a></li>
            <li class="breadcrumb-item"><a href="Interface.Plugins.Datatables.html">Datatables</a></li> --}}
          </ul>
        </nav>
      </div>
      <!-- Title End -->
    </div>
  </div>
  <!-- Title and Top Buttons End -->
@endsection

@section('content')
    <section class="scroll-section" id="bootstrapServerSide">
        {{-- <h2 class="small-title">Bootstrap Server Side</h2> --}}
        <div class="card">
            <div class="card-body">
                <form class="row g-3">
                    <div class="col-md-4">
                        <label>Pekerjaan</label>
                        <label class="form-control">{{ $model->pekerjaan }}</label>
                    </div>

                    <div class="text-right mt-3 col-12">
                        <button type="button" class="btn btn-secondary" onclick="history.back();">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection

@section('js_vendor')

@endsection

@section('js')
<script>

</script>



@endsection