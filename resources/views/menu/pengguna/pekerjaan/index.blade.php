@extends('template.acorn')

@section('title',' User')


@section('css_vendor')
<link rel="stylesheet" href="{{ asset('acorn/css/vendor/datatables.min.css') }}" />

@endsection

@section('css')

@endsection

@section('breadcrumbs')
  <!-- Title and Top Buttons Start -->
  <div class="page-title-container">
    <div class="row">
      <!-- Title Start -->
      <div class="col-12 col-md-7">
        <h1 class="mb-0 pb-0 display-4" id="title">Pekerjaan</h1>
        <nav class="breadcrumb-container d-inline-block" aria-label="breadcrumb">
          <ul class="breadcrumb pt-0">
            <li class="breadcrumb-item"><a href="{{ url('dashboard') }}">Home</a></li>
            <li class="breadcrumb-item"><a href="{{ url('pekerjaan') }}">Pekerjaan</a></li>
          </ul>
        </nav>
      </div>
      <!-- Title End -->

      <!-- Top Buttons Start -->
      <div class="col-12 col-md-5 d-flex align-items-start justify-content-end">
        <!-- Add New Button Start -->
        <a href="{{ route('pekerjaan_create_create') }}" class="btn btn-outline-primary btn-icon btn-icon-start w-100 w-md-auto add-datatable">
          <i data-cs-icon="plus"></i>
          <span>Add New</span>
        </a>
        <!-- Add New Button End -->

        <!-- Check Button Start -->
        <div class="btn-group ms-1 check-all-container">
          <button
            type="button"
            class="btn btn-outline-primary dropdown-toggle dropdown-toggle-split"
            data-bs-offset="0,3"
            data-bs-toggle="dropdown"
            aria-haspopup="true"
            aria-expanded="false"
            data-submenu
          >Other </button>
          <div class="dropdown-menu dropdown-menu-end">
            <div class="dropdown dropstart dropdown-submenu">
              <button class="dropdown-item dropdown-toggle tag-datatable caret-absolute" type="button">Export</button>
              <div class="dropdown-menu">
                <button class="dropdown-item" type="button">Excel</button>
                <button class="dropdown-item" type="button">PDF</button>
                <button class="dropdown-item" type="button">Sale</button>
              </div>
            </div>
          </div>
        </div>
        <!-- Check Button End -->
      </div>
      <!-- Top Buttons End -->
    </div>
  </div>
  <!-- Title and Top Buttons End -->
@endsection

@section('content')
  <!-- Content Start -->
  <div class="data-table-rows slim">

    <!-- Table Start -->
    <div class="data-table-responsive-wrapper">
        <table id="dtServerSide" class="data-table nowrap hover">
          <thead>
            <tr>
              <th class="text-muted text-small text-uppercase">No</th>
              <th class="text-muted text-small text-uppercase">Jenis Pekerjaan</th>
              <th class="text-muted text-small text-uppercase">Action</th>
              {{-- <th class="empty" width="15%">&nbsp;</th> --}}
            </tr>
          </thead>
        </table>
      </div>
    <!-- Table End -->
  </div>
  <!-- Content End -->

@endsection

@section('js_vendor')
<script src="{{ asset('acorn/js/cs/datatable.extend.js') }}"></script>
<script src="{{ asset('acorn/js/plugins/datatable.serverside.js') }}"></script>
<script src="{{ asset('additional/fungsi/crud.js') }}"></script>
@endsection

@section('js')
<script>
    var menu = {!! json_encode($menu->toArray()) !!};

    datatable = {
        table_id : 'dtServerSide',
        url : base_url + '/pekerjaan',
        column: [
            { data: 'id',
                render: function (data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                }
            },
            { data: 'pekerjaan' },
            { data: 'id', //primary key dari tabel
            render: function(data, type, row)
                {
                  console.log(data);
                    let buttonAction =  ` <div class="btn-group">
                        <button type="button" class="btn btn-sm btn-light dropdown-toggle" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          More
                        </button>
                        <div class="dropdown-menu">`;
                            if (menu.allow_view == true) {
                                buttonAction +=   '<a class="dropdown-item" href="'+ row.show +'">Show</a>' ;
                            }
                            if (menu.allow_update == true) {
                             buttonAction +=   '<a class="dropdown-item" href="'+ row.edit +'">Update</a>';
                            }
                            if (menu.allow_delete == true) {
                             buttonAction +=   `<button type="button" class="dropdown-item" onclick="buttonHapus('${data}', 'pekerjaan');">Delete</button>`;
                            }
                    buttonAction += `</div>
                      </div>`;
                   
                
                return buttonAction;
                } 
            }
        ],
        columnDefs: [],
    };
</script>



@endsection