@extends('template.acorn')

@section('title',' User')


@section('css_vendor')
<link rel="stylesheet" href="{{ asset('acorn/css/vendor/datatables.min.css') }}" />

@endsection

@section('css')

@endsection

@section('breadcrumbs')
  <!-- Title and Top Buttons Start -->
  <div class="page-title-container">
    <div class="row">
      <!-- Title Start -->
      <div class="col-12 col-md-7">
        <h1 class="mb-0 pb-0 display-4" id="title">Dashboard</h1>
        <nav class="breadcrumb-container d-inline-block" aria-label="breadcrumb">
          <ul class="breadcrumb pt-0">
            <li class="breadcrumb-item"><a href="{{ url('dashboard') }}">Home</a></li>
          </ul>
        </nav>
      </div>
      <!-- Title End -->

      
    </div>
  </div>
  <!-- Title and Top Buttons End -->
@endsection

@section('content')
  <!-- Content Start -->
  <div class="data-table-rows slim">

   
  </div>
  <!-- Content End -->

@endsection

@section('js_vendor')

@endsection

@section('js')




@endsection